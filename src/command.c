/*
ODKDA: Data Access Library
        Base Command Object
		
Copyright (C) 2006 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdio.h>
#include <malloc.h>
#include <string.h>

#include <odkutils.h>
#include <odkutils_xml.h>

#include <odkda/common.h>
#include <odkda/column.h>
#include <odkda/date.h>
#include <odkda/time.h>
#include <odkda/datetime.h>
#include <odkda/value.h>
#include <odkda/command.h>

int
odk_command_colcount (odkCommand * c)
{
	if (c == NULL)
		return 0;

	return COMMAND (c).colCount;
}

odkColumn **
odk_command_columns (odkCommand * c)
{
	if (c == NULL)
		return NULL;

	return COMMAND (c).columns;
}

int
odk_command_fetch_into (odkCommand * c, odkValue ** row)
{
	if ((c == NULL) || (row == NULL))
		return 0;

	return COMMAND (c).fetch_into (c, row);
}

odkValue **
odk_command_fetch (odkCommand * c)
{
	if (c == NULL)
		return NULL;

	return COMMAND (c).fetch (c);
}

void* odk_command_fetchex(odkCommand * c, int n, odkFieldDef *type){
	void *sptr;
	int i, len;
	
	if (c == NULL)
		return 0;
	
	if (COMMAND (c).fetchex_into == NULL){
		return 0;
	}
	
	len = 0;
	
	for (i = 0; i < n; i++){
		switch(type[i].type){
			case ODKDA_TEXT:
				if (type[i].flags | ODK_STRING_PTR){
					len += sizeof(char*);
				}else{
					len += type[i].size;
				}
				break;
			case ODKDA_INT:
				len += sizeof(int);
				break;
			case ODKDA_FLOAT:
				len += sizeof(float);
				break;
			case ODKDA_DOUBLE:
				len += sizeof(double);
				break;
			case ODKDA_LONG:
				len += sizeof(long);
				break;
			case ODKDA_DATE:
				len += sizeof(odkDate);
				break;
			case ODKDA_TIME:
				len += sizeof(odkTime);
				break;
			case ODKDA_DATETIME:
				len += sizeof(odkDateTime);
				break;
			default:
				break;
		}
	}
	sptr = malloc(len);
	memset(sptr, 0, len);
	
	if (COMMAND (c).fetchex_into(c, n, type, sptr)){
		return sptr;
	}
	
	free(sptr);
	return NULL;
}

int 
odk_command_fetchex_into(odkCommand * c, int n, odkFieldDef *type, void *ptr)
{
	if (c == NULL)
		return 0;
	
	if (COMMAND (c).fetchex_into == NULL){
		return 0;
	}
	
	return COMMAND (c).fetchex_into(c, n, type, ptr);
}

void
odk_command_delete (odkCommand * com)
{
	int i;

	for (i = 0; i < COMMAND (com).colCount; i++) {
		object_unref (COMMAND (com).columns[i]);
	}
	free (COMMAND (com).columns);
}

void
odk_command_init (void *o)
{
	COMMAND (o).type = 0;
	COMMAND (o).state = ODK_STATE_NEW;
	COMMAND (o).private_data = NULL;
	COMMAND (o).type = 0;
	COMMAND (o).columns = NULL;
	COMMAND (o).colCount = 0;
	COMMAND (o).affected = 0;
	COMMAND (o).fetch = NULL;
	COMMAND (o).fetch_into = NULL;
	COMMAND (o).fetchex_into = NULL;
	
	object_append_destructor (o, (FREE_FUNC)&odk_command_delete);
}

odkCommand *
odk_command_new ()
{
	odkCommand *com = malloc (sizeof (odkCommand));
	object_register (com, "odkCommand");
	odk_command_init (com);

	return com;
}
