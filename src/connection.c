/*
ODKDA: Data Access Library
        Connection Object
		
Copyright (C) 2006 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdio.h>
#include <malloc.h>
#include <string.h>

#include <odkutils.h>
#include <odkutils_xml.h>

#include <odkda/common.h>
#include <odkda/column.h>
#include <odkda/date.h>
#include <odkda/time.h>
#include <odkda/datetime.h>
#include <odkda/value.h>
#include <odkda/command.h>
#include <odkda/provider.h>
#include <odkda/connection.h>

void odk_connection_del (odkConnection *conn){
    object_unref(CONNECTION(conn).parent);
}

odkConnection *
odk_connection_new (odkDataProvider * dp)
{
	odkConnection *conn;
	odkLibHandle *l;

	if (dp == NULL) {
		return NULL;
	}
	
	conn = NULL;
	l = DATAPROVIDER (dp).plugin;

	conn = DATAPROVIDER (dp).conn_new ();
	
	if (conn == NULL)
	    return NULL;
	
	object_ref(dp);    
	CONNECTION (conn).parent = dp;
		
	object_append_destructor(conn, (FREE_FUNC)&odk_connection_del);

	CONNECTION (conn).open =
		(FUNC_CONN_OPEN ) odk_library_symbol (l, "plugin_conn_open");
	CONNECTION (conn).close =
		(FUNC_CONN_CLOSE ) odk_library_symbol (l, "plugin_conn_close");
	CONNECTION (conn).execute =
		(FUNC_CONN_EXEC ) odk_library_symbol (l, "plugin_conn_exec");
	
	if (odk_dataprovider_supports(dp, ODKDA_TRANSACTIONS)){
		CONNECTION (conn).begin =
		(FUNC_TRANSAC_FUNC ) odk_library_symbol (l, "plugin_transaction_begin");
		CONNECTION (conn).rollback =
		(FUNC_TRANSAC_FUNC ) odk_library_symbol (l, "plugin_transaction_rollback");
		CONNECTION (conn).commit =
		(FUNC_TRANSAC_FUNC ) odk_library_symbol (l, "plugin_transaction_commit");
	}else{
		CONNECTION (conn).begin = NULL;
		CONNECTION (conn).rollback = NULL;
		CONNECTION (conn).commit = NULL;
	}

	return conn;
}

int
odk_connection_open (odkConnection * conn, char *host, int port,
					 char *user, char *passwd, char *db)
{
	if (conn == NULL)
		return 0;

	return CONNECTION (conn).open (conn, host, port, user, passwd, db);
}

int
odk_connection_close (odkConnection * conn)
{
	if (conn == NULL)
		return 0;

	return CONNECTION (conn).close (conn);
}

int
odk_connection_execute (odkConnection * conn, odkCommand * cmd)
{
	if (conn == NULL)
		return 0;

	if (cmd == NULL)
		return 0;

	return CONNECTION (conn).execute (conn, cmd);
}

int
odk_transaction_begin (odkConnection * conn)
{
	if (conn == NULL)
		return false;

	if (CONNECTION (conn).begin == NULL)
		return false;

	return CONNECTION (conn).begin (conn);
}

int
odk_transaction_rollback (odkConnection * conn)
{
	if (conn == NULL)
		return false;

	if (CONNECTION (conn).rollback == NULL)
		return false;

	return CONNECTION (conn).rollback (conn);
}

int
odk_transaction_commit (odkConnection * conn)
{
	if (conn == NULL)
		return false;

	if (CONNECTION (conn).commit == NULL)
		return false;

	return CONNECTION (conn).commit (conn);
}
