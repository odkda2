/*
ODKDA: Data Access Library
        Column Object
		
Copyright (C) 2006 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdio.h>
#include <malloc.h>
#include <string.h>

#include <odkutils.h>
#include <odkutils_xml.h>

#include <odkda/common.h>
#include <odkda/column.h>

void
odk_column_delete (odkColumn * c)
{
	free (c->name);
}

odkColumn *
odk_column_new (char *name)
{
	odkColumn *c = (odkColumn *) malloc (sizeof (odkColumn));

	object_register (c, "odkColumn");
	object_append_destructor (c, (FREE_FUNC)&odk_column_delete);
	c->name = strcopy (name);
	c->type = 0;
	c->maxSize = 0;
	c->flags = 0;

	return c;
}

odkColumn *
odk_column_new1(char *name, int type, int flags)
{
	odkColumn *c = (odkColumn *) malloc (sizeof (odkColumn));

	object_register (c, "odkColumn");
	object_append_destructor (c, (FREE_FUNC)&odk_column_delete);
	c->name = strcopy (name);
	c->type = type;
	c->maxSize = 0;
	c->flags = flags;

	return c;
}

int odk_column_get_type(odkColumn *c){
	if (c == NULL)
		return 0;
	
	return c->type;
}

int odk_column_get_flags(odkColumn *c){
	if (c == NULL)
		return 0;
	
	return c->flags;
}

void odk_column_set_type(odkColumn *c, int type){
	if (c == NULL)
		return ;
	
	c->type = type;
}

void odk_column_set_flags(odkColumn *c, int flags){
	if (c == NULL)
		return ;
	
	c->flags = flags;
}
