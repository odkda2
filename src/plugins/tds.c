/*
ODKDA: Data Access Library
        FreeTDS Plugin
		
Copyright (C) 2006 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>

#include <odkutils.h>
#include <odkutils_interop.h>
#include <odkutils_xml.h>

#include <odkda2.h>
#include <sybdb.h>

typedef struct {
	CLASS_CONNECTION DBPROCESS *dblink;
} odkTDSConnection;

typedef struct {
	DBPROCESS *dblink;
} cmdData;

int
sqlite2odk (int d)
{
	switch (d) {
	case SYBINT1:
		return ODKDA_CHAR;
	case SYBINT2:
		return ODKDA_SHORT;
	case SYBINT4:
		return ODKDA_INT;
	case SYBINT8:
		return ODKDA_LONG;
	case SYBFLTN:
		return ODKDA_FLOAT;
	case SYBFLT8:
		return ODKDA_DOUBLE;
	case SYBVARCHAR:
	case SYBCHAR:
	case SYBTEXT:
		return ODKDA_TEXT;
	case SYBBINARY:
	case SYBVARBINARY:
		return ODKDA_BLOB;
	default:
		return 0;
	}
}

int
plugin_conn_open (odkTDSConnection * conn, char *host, int port,
				  char *user, char *passwd, char *db)
{
	DBPROCESS *dblink;
	LOGINREC *log;
	int retcode;

	if (conn->dblink != NULL)
		return 0;

	retcode = dbinit ();

	if (retcode == FAIL)
		return 0;

	log = dblogin ();

	dbsetlname (log, user, DBSETUSER);
	dbsetlname (log, passwd, DBSETPWD);

	dblink = dbopen (log, host);
	dbloginfree (log);

	dbuse (dblink, db);
	conn->dblink = dblink;

	return 1;
}

int
plugin_conn_close (odkTDSConnection * conn)
{
	if (conn->dblink == NULL)
		return 0;

	dbclose (conn->dblink);
	return 1;
}

odkValue **
plugin_command_fetch (odkCommand * com)
{
	cmdData *d;
	odkValue **v;
	int ret, i;
	char *tmp;

	if (com == NULL) {
		return NULL;
	}

	d = COMMAND (com).private_data;

	ret = dbnextrow (d->dblink);

	v = (odkValue **) malloc (sizeof (odkValue *) * COMMAND (com).colCount);

	for (i = 0; i < COMMAND (com).colCount; i++) {
		v[i] = odk_value_new (COMMAND (com).columns[i]->type);
		tmp = (char*)dbdata(d->dblink, i+1);
		v[i]->size = dbcollen(d->dblink, i+1);
		switch (v[i]->type) {
		case ODKDA_TEXT:
			v[i]->vString = tmp;
			break;
		case ODKDA_INT:
			break;
		case ODKDA_FLOAT:
			v[i]->size = 4;
			v[i]->vFloat = 0;
		case ODKDA_BLOB:
			break;
		}
	}

	return v;
}

int
plugin_command_fetch_into (void *com, odkValue ** row)
{
	return 0;
}

int
plugin_transaction_begin (odkTDSConnection * conn)
{
	return 0;
}

int
plugin_transaction_rollback (odkTDSConnection * conn)
{
	return 0;
}

int
plugin_transaction_commit (odkTDSConnection * conn)
{
	return 0;
}

int
plugin_conn_free (odkTDSConnection * conn)
{
	return 0;
}

int
plugin_conn_exec_sql (odkTDSConnection * conn, odkCommand * cmd)
{
	cmdData *d;
	void *tmp;
	char *sql;
	int i, retcode;
	odkLibHandle *l = DATAPROVIDER (CONNECTION (conn).parent).plugin;

	if (COMMAND (cmd).private_data != NULL)
		return 0;

	sql = odk_sql_prepare((odkSQLCommand*)cmd);
	
	if (sql == NULL)
		return 0;
	
	d = malloc (sizeof (cmdData));

	retcode = dbcmd (conn->dblink, SQLCOMMAND (cmd).sql);
	
	free(sql);

	if (retcode == FAIL) {
		free (d);
		return 0;
	}

	retcode = dbsqlsend (conn->dblink);

	if (retcode == FAIL) {
		free (d);
		return 0;
	}

	d->dblink = conn->dblink;

	COMMAND (cmd).private_data = d;
	COMMAND (cmd).fetch =
		(FUNC_COM_FETCH *) odk_library_symbol (l, "plugin_command_fetch");
	COMMAND (cmd).fetch_into =
		(FUNC_COM_FETCHINTO *) odk_library_symbol (l,
												   "plugin_command_fetch_into");
	COMMAND (cmd).colCount = 0;
	COMMAND (cmd).columns = NULL;

	retcode = dbresults (conn->dblink);

	if (retcode == 0) {
		COMMAND (cmd).affected = 0;
		COMMAND (cmd).private_data = NULL;
		free (d);
		return 1;
	}

	COMMAND (cmd).colCount = retcode;
	COMMAND (cmd).columns =
		malloc (sizeof (odkColumn *) * COMMAND (cmd).colCount);

	for (i = 0; i < COMMAND (cmd).colCount; i++) {
		COMMAND (cmd).columns[i] =
			odk_column_new ((char *) dbcolname (conn->dblink, i+1));
		COLUMN (COMMAND (cmd).columns[i]).type =
			tds2odk (dbcoltype (conn->dblink, i+1));
	}

	COMMAND (cmd).affected = -1;

	return 1;
}

int
plugin_conn_exec (odkTDSConnection * conn, odkCommand * cmd)
{
	if (COMMAND (cmd).type == 1)
		return plugin_conn_exec_sql (conn, cmd);

	return 0;
}

void
plugin_provider_supports (int *f, int *sqlf, odkSList * exts)
{
	*f = ODKDA_SQL;
	*sqlf = 0;
}

odkTDSConnection *
plugin_connection_new ()
{
	odkTDSConnection *conn =
		(odkTDSConnection *) malloc (sizeof (odkTDSConnection));
	odkLibHandle *l;
	object_register (conn, "odkTDSConnection");
	object_append_destructor (conn, (FREE_FUNC)&plugin_conn_free);

	conn->dblink = NULL;
}
