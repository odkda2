/*
ODKDA: Data Access Library
        SQLite3 Plugin
		
Copyright (C) 2006 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>

#include <odkutils.h>
#include <odkutils_interop.h>
#include <odkutils_xml.h>

#include <odkda2.h>
#include <sqlite.h>

typedef struct {
	CLASS_CONNECTION sqlite *dblink;
} odkSQLiteConnection;

typedef struct {
	sqlite_vm *vm;
	int state;
} cmdData;

int
plugin_conn_open (odkSQLiteConnection * conn, char *host, int port,
				  char *user, char *passwd, char *db)
{
	sqlite *dblink;

	if (conn->dblink != NULL)
		return 0;

	sqlite_open (db, &dblink);
	conn->dblink = dblink;

	return 1;
}

int
plugin_conn_close (odkSQLiteConnection * conn)
{
	if (conn->dblink == NULL)
		return 0;

	sqlite_close (conn->dblink);
	return 1;
}

int
plugin_command_fetchex (odkCommand * com)
{
	cmdData *d;
	int i, ptrpos, tmpi;
	odkFieldDef *defines;
	void *ptr;
	char *tmps;
	
	printf("Plugin Fetchex\n");
	
	if (COMMAND (com).dtype == 0)
		return 0;
	
	printf("Command type: %i\n", COMMAND (com).dtype);
	
	d = (cmdData *) COMMAND (com).private_data;

	if (d == NULL)
		return 0;

	if (d->state == SQLITE_DONE) {
		sqlite_finalize (d->vm);
		COMMAND (com).private_data = NULL;
		free (d);
		return 0;
	}
	
	ptr = COMMAND (com).sptr;
	defines = COMMAND (com).defines_types;
	
	ptrpos = 0;
	ctype = 0;
	
	for (i = 0; i < COMMAND (com).colCount; i++) {
		printf("Define[%i].type = %i\n", i, defines[i].type);
		printf("Pointer at %x + offset %i\n", ptr, ptrpos);
		
		switch (defines[i].type) {
		case ODKDA_TEXT:
			printf("Got Text!\n");
			if (defines[i].flags | ODK_STRING_PTR){
				printf("Using String Pointers\n");
				
				tmps = strcopy ((char *) sqlite3_column_text (d->vm, i));
				printf("TextValue: %s\n", tmps);
				(char*)(sptr+ptrpos) = tmps;
				
				ptrpos+=sizeof(char*);
			}else{
				memset((void*)(sptr+ptrpos), 0, defines[i].size);
				tmpi = sqlite3_column_bytes (d->vm, i);
				tmps = (char *) sqlite3_column_text (d->vm, i);
				memcpy((void*)(sptr+ptrpos), tmps, tmpi);
				ptrpos += defines[i].size;
			}
			break;
		case ODKDA_INT:
			*(int*)(sptr+ptrpos) = sqlite3_column_int (d->vm, i);
			ptrpos+=sizeof(int);
			break;
		case ODKDA_FLOAT:
			*(float*)(sptr+ptrpos) = sqlite3_column_double (d->vm, i);
			ptrpos+=sizeof(float);
			break;
		case ODKDA_DOUBLE:
			*(double*)(sptr+ptrpos) = sqlite3_column_double (d->vm, i);
			ptrpos+=sizeof(double);
			break;
		}
	}

	d->state = sqlite3_step (d->vm);
	return 1;
}

odkValue **
plugin_command_fetch (odkCommand * com)
{
	cmdData *d;
	odkValue **v;
	int ret, i;

	d = (cmdData *) COMMAND (com).private_data;

	if (d == NULL)
		return NULL;

	if (d->state == SQLITE_DONE) {
		sqlite_finalize (d->vm);
		COMMAND (com).private_data = NULL;
		free (d);
		return NULL;
	}

	v = (odkValue **) malloc (sizeof (odkValue *) * COMMAND (com).colCount);

	for (i = 0; i < COMMAND (com).colCount; i++) {
		v[i] = odk_value_new (COMMAND (com).columns[i]->type);
		switch (v[i]->type) {
		case ODKDA_TEXT:
			v[i]->size = sqlite3_column_bytes (d->vm, i);
			v[i]->vString = strcopy ((char *) sqlite3_column_text (d->vm, i));
			break;
		case ODKDA_INT:
			v[i]->size = 4;
			v[i]->vInt = sqlite3_column_int (d->vm, i);
			break;
		case ODKDA_FLOAT:
			v[i]->size = 4;
			v[i]->vFloat = sqlite3_column_double (d->vm, i);
		case ODKDA_BLOB:
			v[i]->size = sqlite3_column_bytes (d->vm, i);
			v[i]->vString = (char *) sqlite3_column_blob (d->vm, i);
			break;
		}
	}

	d->state = sqlite3_step (d->vm);
	return v;
}

int
plugin_command_fetch_into (void *com, odkValue ** row)
{
	cmdData *d;
	odkValue **v;
	int ret, i;

	d = (cmdData *) COMMAND (com).private_data;

	if (d == NULL)
		return 0;

	if (d->state == SQLITE_DONE) {
		sqlite3_finalize (d->vm);
		COMMAND (com).private_data = NULL;
		free (d);
		return 0;
	}

	v = row;

	for (i = 0; i < COMMAND (com).colCount; i++) {
		v[i]->type = COMMAND (com).columns[i]->type;
		switch (v[i]->type) {
		case ODKDA_TEXT:
			v[i]->size = sqlite3_column_bytes (d->vm, i);
			v[i]->vString = strcopy ((char *) sqlite3_column_text (d->vm, i));
			break;
		}
	}

	d->state = sqlite3_step (d->vm);
	return 1;
}

int
odkda_transaction_begin (odkSQLiteConnection * conn)
{
	if (sqlite3_exec (conn->dblink, "BEGIN TRANSACTION", NULL, NULL, NULL)
		== SQLITE_OK)
		return 1;

	return 0;
}

int
odkda_transaction_rollback (odkSQLiteConnection * conn)
{
	if (sqlite3_exec (conn->dblink, "ROLLBACK", NULL, NULL, NULL) == SQLITE_OK)
		return 1;

	return 0;
}

int
odkda_transaction_commit (odkSQLiteConnection * conn)
{
	if (sqlite3_exec (conn->dblink, "COMMIT", NULL, NULL, NULL) == SQLITE_OK)
		return 1;

	return 0;
}

int
plugin_conn_free (odkSQLiteConnection * conn)
{

}

int
plugin_conn_exec_sql (odkSQLiteConnection * conn, odkCommand * cmd)
{
	cmdData *d;
	void *tmp;
	int i;
	LIBHANDLE *l = DATAPROVIDER (CONNECTION (conn).parent).plugin;

	if (COMMAND (cmd).private_data != NULL)
		return 0;
	
	d = malloc (sizeof (cmdData));

	sqlite_prepare (conn->dblink, SQLCOMMAND (cmd).sql,
					 strlen (SQLCOMMAND (cmd).sql), &(d->vm),
					 (const char **) &tmp);

	COMMAND (cmd).private_data = d;
	COMMAND (cmd).fetch =
		(FUNC_COM_FETCH *) odk_library_symbol (l, "plugin_command_fetch");
	COMMAND (cmd).fetchex =
		(FUNC_COM_FETCHEX *) odk_library_symbol (l, "plugin_command_fetchex");
	COMMAND (cmd).fetch_into =
		(FUNC_COM_FETCHINTO *) odk_library_symbol (l,
												   "plugin_command_fetch_into");
	COMMAND (cmd).colCount = 0;
	COMMAND (cmd).columns = NULL;

	d->state = sqlite_step (d->vm);
	if (d->state == SQLITE_ERROR)
		return 0;
	if (d->state == SQLITE_DONE) {
		COMMAND (cmd).affected = sqlite_changes (conn->dblink);
		sqlite_finalize (d->vm);
		COMMAND (cmd).private_data = NULL;
		free (d);
		return 1;
	}

	COMMAND (cmd).colCount = sqlite_column_count (d->vm);
	COMMAND (cmd).rowCount = 0;
	COMMAND (cmd).columns =
		malloc (sizeof (odkColumn *) * COMMAND (cmd).colCount);

	for (i = 0; i < COMMAND (cmd).colCount; i++) {
		COMMAND (cmd).columns[i] =
			odk_column_new ((char *) sqlite3_column_name (d->vm, i));
		COLUMN (COMMAND (cmd).columns[i]).type = ODKDA_TEXT
	}

	COMMAND (cmd).affected = -1;

	return 1;
}

int
plugin_conn_exec (odkSQLiteConnection * conn, odkCommand * cmd)
{
	if (COMMAND (cmd).type == 1)
		return plugin_conn_exec_sql (conn, cmd);

	return 0;
}

int
plugin_provider_init (odkDataProvider *dp)
{
	odkBitfield features;
	odkBitfield sqlfeatures;

	features = DATAPROVIDER(dp).features;
	sqlfeatures = DATAPROVIDER(dp).sqlfeatures;

	odk_bitfield_set(features, ODKDA_SQL);
	odk_bitfield_set(features, ODKDA_TRANSACTIONS);
	odk_bitfield_set(features, ODKDA_FILEDB);

	return 1;
}

odkSQLiteConnection *
plugin_connection_new ()
{
	odkSQLiteConnection *conn =
		(odkSQLiteConnection *) malloc (sizeof (odkSQLiteConnection));
	LIBHANDLE l;
	object_register (conn, "odkSQLite2Connection");
	object_append_destructor (conn, (FREE_FUNC)&plugin_conn_free);

	conn->dblink = NULL;
}
