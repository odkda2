/*
ODKDA: Data Access Library
        MySQL Plugin
		
Copyright (C) 2006 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>

#include <odkutils.h>
#include <odkutils_xml.h>

#include <odkda2.h>
#include <mysql.h>


typedef struct {
	CLASS_CONNECTION MYSQL *dblink;
} odkMySQLConnection;

int
enum2odk (int d)
{
	switch (d) {
	case FIELD_TYPE_BLOB:
		return ODKDA_BLOB;
	case FIELD_TYPE_DOUBLE:
		return ODKDA_DOUBLE;
	case FIELD_TYPE_TINY:
		return ODKDA_CHAR;
	case FIELD_TYPE_SHORT:
		return ODKDA_SHORT;
	case FIELD_TYPE_INT24:
		return ODKDA_INT;
	case FIELD_TYPE_LONG:
		return ODKDA_INT;
	case FIELD_TYPE_LONGLONG:
		return ODKDA_LONG;
	case FIELD_TYPE_DECIMAL:
	//case FIELD_TYPE_NEWDECIMAL:
	case FIELD_TYPE_FLOAT:
		return ODKDA_FLOAT;
	case MYSQL_TYPE_DATE:
		return ODKDA_DATE;
	case MYSQL_TYPE_TIME:
		return ODKDA_TIME;
	case MYSQL_TYPE_DATETIME:
		return ODKDA_DATETIME;
	case FIELD_TYPE_STRING:
	case FIELD_TYPE_VAR_STRING:
	default:
		return ODKDA_TEXT;
	}
	return 0;
}

odkSList *
ext_table_list (odkMySQLConnection * conn)
{
	MYSQL_RES *table_res;
	odkSList *ret;
	MYSQL_ROW row;
	unsigned long *lengths;
	char *tmp;

	if (conn == NULL)
		return NULL;

	ret = odk_slist_new(0);
	table_res = mysql_list_tables(conn->dblink, NULL);
	
	while(row = mysql_fetch_row(table_res)){
		lengths = mysql_fetch_lengths (table_res);
		tmp = malloc (lengths[0] + 1);
		memset (tmp, 0, lengths[0] + 1);
		memcpy (tmp, row[0], lengths[0]);
		odk_slist_append(ret, tmp);
	}

	mysql_free_result (table_res);

	return ret;
}

int 
ext_change_db (odkMySQLConnection * conn, char *db)
{
	if (conn == NULL)
		return 0;

	#ifdef mysql_change_db
	return mysql_change_db(conn->dblink, db);
	#else
		return false;
	#endif
}


plugin_conn_open (odkMySQLConnection * conn, char *host, int port,
				  char *user, char *passwd, char *db)
{
	MYSQL *dblink;

	dblink = mysql_init (NULL);

	if (dblink == NULL) {
		return 0;
	}

	if (host[0] == '/') {
		mysql_real_connect (dblink, NULL, user, passwd, db, 0, host, 0);
	} else {
		mysql_real_connect (dblink, host, user, passwd, db, port, NULL, 0);
	}

	conn->dblink = dblink;

	return 1;
}

int
plugin_conn_close (odkMySQLConnection * conn)
{
	MYSQL *dblink = conn->dblink;

	if (dblink == NULL)
		return 0;

	mysql_close (dblink);
	conn->dblink = NULL;
	return 1;
}

int
plugin_command_free (odkCommand * cmd)
{
	MYSQL_RES *res;

	res = (MYSQL_RES *) COMMAND (cmd).private_data;

	mysql_free_result (res);
	return true;
}

int
plugin_command_fetchex_into (odkCommand * com, int n, odkFieldDef *type, void *ptr)
{
	MYSQL_RES *res;
	MYSQL_ROW row;
	odkValue **v;
	int i;
	unsigned long *lengths;
	char tmp[256];
	odkDate *d; 
	odkTime *t; 
	odkDateTime *dt;
	int ptrpos, tmpi;
	odkFieldDef *defines;
	char *tmps;
#ifdef WIN32
	int sptr = (int)ptr;
#else
	void *sptr = ptr;
#endif

	res = (MYSQL_RES *) COMMAND (com).private_data;

	if (res == NULL)
		return 0;

	if (!(row = mysql_fetch_row (res))) {
		mysql_free_result (res);
		COMMAND (com).private_data = NULL;
		return 0;
	}

	defines = type;	
	ptrpos = 0;

	lengths = mysql_fetch_lengths (res);
	for (i = 0; i < COMMAND (com).colCount; i++) {		
		switch (defines[i].type) {
		case ODKDA_TEXT:
			tmpi = lengths[i];
			if (defines[i].flags | ODK_STRING_PTR){
				tmps = (char*)malloc (lengths[i] + 1);
				memset (tmps, 0, lengths[i] + 1);
				memcpy (tmps, row[i], lengths[i]);
				
				*(char**)(sptr+ptrpos) = tmps;
				
				ptrpos+=sizeof(char*);
			}else{
				memset((void*)(sptr+ptrpos), 0, defines[i].size);
				if (lengths[i] > defines[i].size){
					tmpi = defines[i].size;
				}else{
					tmpi = lengths[i];
				}
				memcpy((void*)(sptr+ptrpos), row[i], tmpi);
				ptrpos += tmpi;
			}
			break;
		case ODKDA_INT:
			memset (tmp, 0, 256);
			memcpy (tmp, row[i], lengths[i]);
			*(int*)(sptr+ptrpos) = atoi (tmp);
			ptrpos+=sizeof(int);
			break;
		case ODKDA_FLOAT:
			memset (tmp, 0, 256);
			memcpy (tmp, row[i], lengths[i]);
			*(float*)(sptr+ptrpos) = (float) strtod (tmp, NULL);
			ptrpos+=sizeof(float);
			break;
		case ODKDA_DOUBLE:
			memset (tmp, 0, 256);
			memcpy (tmp, row[i], lengths[i]);
			*(double*)(sptr+ptrpos) = strtod (tmp, NULL);
			ptrpos+=sizeof(float);
			break;
		case ODKDA_BLOB:
			/*v[i]->size = lengths[i];
			v[i]->vBlob = malloc (lengths[i] + 1);
			memset (v[i]->vBlob, 0, lengths[i] + 1);
			memcpy (v[i]->vBlob, row[i], lengths[i]);*/
			break;
		case ODKDA_DATE:
			if (lengths[i] != 10)
				break;
			
			d = (odkDate*)(sptr+ptrpos);
			
			memset (tmp, 0, 10);
			memcpy (tmp, row[i], 4);
			d->year = atoi(tmp);
			
			memset (tmp, 0, 10);
			memcpy (tmp, (row[i] + 5), 2);
			d->month = atoi(tmp);
			
			memset (tmp, 0, 10);
			memcpy (tmp, (row[i] + 8), 2);
			d->day = atoi(tmp);
			
			ptrpos += sizeof(odkDate);
			break;
		case ODKDA_TIME:
			if (lengths[i] != 8)
				break;
			
			t = (odkTime*)(sptr+ptrpos);
			
			memset (tmp, 0, 10);
			memcpy (tmp, row[i], 2);
			t->hours = atoi(tmp);
			
			memset (tmp, 0, 10);
			memcpy (tmp, (row[i] + 3), 2);
			t->minutes = atoi(tmp);
			
			memset (tmp, 0, 10);
			memcpy (tmp, (row[i] + 6), 2);
			t->seconds = atoi(tmp);
			
			ptrpos += sizeof(odkTime);
			break;
		case ODKDA_DATETIME:
			dt = (odkDateTime*)(sptr+ptrpos);
			
			memset (tmp, 0, 10);
			memcpy (tmp, row[i], 4);
			dt->year = atoi(tmp);
			
			memset (tmp, 0, 10);
			memcpy (tmp, (row[i] + 5), 2);
			dt->month = atoi(tmp);
			
			memset (tmp, 0, 10);
			memcpy (tmp, (row[i] + 8), 2);
			dt->day = atoi(tmp);
			
			memset (tmp, 0, 10);
			memcpy (tmp, (row[i] + 11), 2);
			dt->hours = atoi(tmp);
			
			memset (tmp, 0, 10);
			memcpy (tmp, (row[i] + 14), 2);
			dt->minutes = atoi(tmp);
			
			memset (tmp, 0, 10);
			memcpy (tmp, (row[i] + 17), 2);
			dt->seconds = atoi(tmp);
			
			ptrpos += sizeof(odkDateTime);
			break;
		}
	}

	return 1;
}

odkValue **
plugin_command_fetch (odkCommand * com)
{
	MYSQL_RES *res;
	MYSQL_ROW row;
	odkValue **v;
	int i;
	unsigned long *lengths;
	char tmp[256];
	odkDate *d; 
	odkTime *t; 
	odkDateTime *dt;

	res = (MYSQL_RES *) COMMAND (com).private_data;

	if (res == NULL)
		return NULL;

	if (!(row = mysql_fetch_row (res))) {
		mysql_free_result (res);
		COMMAND (com).private_data = NULL;
		return NULL;
	}

	v = (odkValue**)malloc (sizeof (odkValue *) * COMMAND (com).colCount);

	lengths = mysql_fetch_lengths (res);
	for (i = 0; i < COMMAND (com).colCount; i++) {		
		v[i] = odk_value_new (COMMAND (com).columns[i]->type);
		switch (v[i]->type) {
		case ODKDA_TEXT:
			v[i]->size = lengths[i];
			v[i]->vString = malloc (lengths[i] + 1);
			memset (v[i]->vString, 0, lengths[i] + 1);
			memcpy (v[i]->vString, row[i], lengths[i]);
			break;
		case ODKDA_INT:
			memset (tmp, 0, 256);
			memcpy (tmp, row[i], lengths[i]);
			v[i]->vInt = atoi (tmp);
			break;
		case ODKDA_FLOAT:
			memset (tmp, 0, 256);
			memcpy (tmp, row[i], lengths[i]);
			v[i]->vFloat = (float) strtod (tmp, NULL);
			break;
		case ODKDA_DOUBLE:
			memset (tmp, 0, 256);
			memcpy (tmp, row[i], lengths[i]);
			v[i]->vDouble = strtod (tmp, NULL);
			break;
		case ODKDA_BLOB:
			v[i]->size = lengths[i];
			v[i]->vBlob = malloc (lengths[i] + 1);
			memset (v[i]->vBlob, 0, lengths[i] + 1);
			memcpy (v[i]->vBlob, row[i], lengths[i]);
			break;
		case ODKDA_DATE:
			if (lengths[i] != 10)
				break;
			
			d = (odkDate*)malloc(sizeof(odkDate));
			
			memset (tmp, 0, 10);
			memcpy (tmp, row[i], 4);
			d->year = atoi(tmp);
			
			memset (tmp, 0, 10);
			memcpy (tmp, (row[i] + 5), 2);
			d->month = atoi(tmp);
			
			memset (tmp, 0, 10);
			memcpy (tmp, (row[i] + 8), 2);
			d->day = atoi(tmp);
			
			v[i]->size = sizeof(odkDate);
			v[i]->vDate = d;
			break;
		case ODKDA_TIME:
			if (lengths[i] != 8)
				break;
			
			t = (odkTime*)malloc(sizeof(odkTime));
			
			memset (tmp, 0, 10);
			memcpy (tmp, row[i], 2);
			t->hours = atoi(tmp);
			
			memset (tmp, 0, 10);
			memcpy (tmp, (row[i] + 3), 2);
			t->minutes = atoi(tmp);
			
			memset (tmp, 0, 10);
			memcpy (tmp, (row[i] + 6), 2);
			t->seconds = atoi(tmp);
			
			v[i]->size = sizeof(odkTime);
			v[i]->vTime = t;
			break;
		case ODKDA_DATETIME:
			dt = (odkDateTime*)malloc(sizeof(odkDateTime));
			
			memset (tmp, 0, 10);
			memcpy (tmp, row[i], 4);
			dt->year = atoi(tmp);
			
			memset (tmp, 0, 10);
			memcpy (tmp, (row[i] + 5), 2);
			dt->month = atoi(tmp);
			
			memset (tmp, 0, 10);
			memcpy (tmp, (row[i] + 8), 2);
			dt->day = atoi(tmp);
			
			memset (tmp, 0, 10);
			memcpy (tmp, (row[i] + 11), 2);
			dt->hours = atoi(tmp);
			
			memset (tmp, 0, 10);
			memcpy (tmp, (row[i] + 14), 2);
			dt->minutes = atoi(tmp);
			
			memset (tmp, 0, 10);
			memcpy (tmp, (row[i] + 17), 2);
			dt->seconds = atoi(tmp);
			
			v[i]->size = sizeof(odkDateTime);
			v[i]->vDateTime = dt;
			break;
		}
	}

	return v;

}

int
plugin_command_fetch_into (void *com, odkValue ** row)
{
	return false;
}

int
plugin_conn_free (odkMySQLConnection * conn)
{
	return true;
}

int
plugin_conn_exec_sql (odkMySQLConnection * conn, odkCommand * cmd)
{
	MYSQL *dblink = conn->dblink;
	MYSQL_RES *res;
	MYSQL_FIELD *myfields;
	char *sql;
	odkDataProvider *dp = CONNECTION (conn).parent;
	int i;
	odkLibHandle *lib = DATAPROVIDER (dp).plugin;

	if (COMMAND (cmd).private_data != NULL)
		return 0;

	sql = odk_sql_prepare((odkSQLCommand*)cmd);
	
	if (sql == NULL)
		return 0;
	
	if (mysql_query (dblink, sql) != 0)
		return 0;

	free(sql);
	
	if (mysql_field_count(dblink) == 0)
		return 1;
	
	res = mysql_store_result (dblink);
	myfields = mysql_fetch_fields (res);

	COMMAND (cmd).private_data = res;
	COMMAND (cmd).colCount = mysql_num_fields (res);
	COMMAND (cmd).rowCount = mysql_num_rows (res);
	COMMAND (cmd).fetch =
		(FUNC_COM_FETCH ) odk_library_symbol (lib, "plugin_command_fetch");
	COMMAND (cmd).fetchex_into =
		(FUNC_COM_FETCHEX_INTO ) odk_library_symbol (lib, "plugin_command_fetchex_into");
	COMMAND (cmd).fetch_into =
		(FUNC_COM_FETCHINTO ) odk_library_symbol (lib, "plugin_command_fetch_into");
	COMMAND (cmd).affected = mysql_affected_rows (dblink);

	if (res == NULL) {
		if (mysql_field_count (dblink) == 0) {
			COMMAND (cmd).private_data = NULL;
			COMMAND (cmd).columns = NULL;
			return 1;
		}
	}

	COMMAND (cmd).columns =
		malloc (sizeof (odkColumn *) * COMMAND (cmd).colCount);

	for (i = 0; i < COMMAND (cmd).colCount; i++) {
		COMMAND (cmd).columns[i] = odk_column_new (myfields[i].name);
		COLUMN (COMMAND (cmd).columns[i]).type = enum2odk (myfields[i].type);
	}

	return 1;
}

int
plugin_conn_exec (odkMySQLConnection * conn, odkCommand * cmd)
{
	if (COMMAND (cmd).type == 1)
		return plugin_conn_exec_sql (conn, cmd);

	return -1;
}

int 
plugin_transaction_begin (odkMySQLConnection * conn)
{
	int ret;

	ret = mysql_query(conn->dblink, "BEGIN TRANSACTION");

	if (ret == 0)
		return 1;

	return 0;
}

int 
plugin_transaction_rollback (odkMySQLConnection * conn)
{
	return mysql_rollback(conn->dblink);
}

int 
plugin_transaction_commit (odkMySQLConnection * conn)
{
	return mysql_commit(conn->dblink);
}

int
plugin_provider_init (odkDataProvider *dp)
{
	odkBitfield features;
	odkBitfield sqlfeatures;
	odkSList *extensions;

	features = DATAPROVIDER(dp).features;
	sqlfeatures = DATAPROVIDER(dp).sqlfeatures;
	extensions = DATAPROVIDER(dp).extensions;

	odk_bitfield_set(features, ODKDA_SQL);
	odk_bitfield_set(features, ODKDA_TRANSACTIONS);
	odk_bitfield_set(features, ODKDA_ROWCOUNT);

	odk_slist_append(extensions, strcopy("ext_table_list"));
	#ifdef mysql_change_db
	odk_slist_append(extensions, strcopy("ext_change_db"));
	#endif

	return 1;
}

odkMySQLConnection *
plugin_connection_new ()
{
	odkMySQLConnection *conn =
		(odkMySQLConnection *) malloc (sizeof (odkMySQLConnection));

	object_register (conn, "odkMySQLConnection");
	//object_append_destructor (conn, (FREE_FUNC)&plugin_conn_free);

	conn->dblink = NULL;

	return conn;
}
