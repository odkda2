/*
ODKDA: Data Access Library
        PostgreSQL Plugin
		
Copyright (C) 2006 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>

#include <odkutils.h>
#include <odkutils_xml.h>

#include <odkda2.h>
#include <libpq-fe.h>

/* Definitions taken from PostgreSQL sources */
#define BOOLOID                 16
#define BYTEAOID                17
#define CHAROID                 18
#define NAMEOID                 19
#define INT8OID                 20
#define INT2OID                 21
#define INT2VECTOROID   		22
#define INT4OID                 23
#define REGPROCOID              24
#define TEXTOID                 25
#define OIDOID                  26
#define TIDOID          		27
#define XIDOID 					28
#define CIDOID 					29
#define OIDVECTOROID    		30
#define POINTOID                600
#define LSEGOID                 601
#define PATHOID                 602
#define BOXOID                  603
#define POLYGONOID              604
#define LINEOID                 628
#define FLOAT4OID 				700
#define FLOAT8OID 				701
#define ABSTIMEOID              702
#define RELTIMEOID              703
#define TINTERVALOID    		704
#define UNKNOWNOID              705
#define CIRCLEOID               718
#define CASHOID 790
#define MACADDROID 829
#define INETOID 869
#define CIDROID 650
#define ACLITEMOID              1033
#define BPCHAROID               1042
#define VARCHAROID              1043
#define DATEOID                 1082
#define TIMEOID                 1083
#define TIMESTAMPOID    1114
#define TIMESTAMPTZOID  1184
#define INTERVALOID             1186
#define TIMETZOID               1266
#define BITOID   1560
#define VARBITOID         1562
#define NUMERICOID              1700
#define REFCURSOROID    1790
#define REGPROCEDUREOID 2202
#define REGOPEROID              2203
#define REGOPERATOROID  2204
#define REGCLASSOID             2205
#define REGTYPEOID              2206
#define RECORDOID               2249
#define CSTRINGOID              2275
#define ANYOID                  2276
#define ANYARRAYOID             2277
#define VOIDOID                 2278
#define TRIGGEROID              2279
#define LANGUAGE_HANDLEROID             2280
#define INTERNALOID             2281
#define OPAQUEOID               2282
#define ANYELEMENTOID   2283


typedef struct {
	CLASS_CONNECTION 
	PGconn *dblink;
} odkPGConnection;

typedef struct {
	PGresult *res;
	int c;
} cmdData;

int
pg2odk (int d)
{
	switch (d) {
	case INT2OID:
	case INT4OID:
		return ODKDA_INT;
	case INT8OID:
		return ODKDA_LONG;
	case FLOAT4OID:
		return ODKDA_FLOAT;
	case FLOAT8OID:
		return ODKDA_DOUBLE;
	case VARCHAROID:
		return ODKDA_TEXT;
	case DATEOID:
		return ODKDA_DATE;
	case TIMEOID:
		return ODKDA_TIME;
	default:
		return 0;
	}
}

odkSList *
ext_table_list (odkPGConnection * conn)
{
	odkSList *ret;
	PGresult *res;
	int c, i;
	
	ret = odk_slist_new(0);
	
	res = PQexec(conn->dblink, "SELECT tablename from pg_tables where schemaname='public'");

	c = PQntuples(res);
	
	for (i = 0; i < c; i++){
		odk_slist_append(ret, strcopy(PQgetvalue (res, i, 0)));
	}
	
	PQclear (res);

	return ret;
}

int
plugin_conn_open (odkPGConnection * conn, char *host, int port,
				  char *user, char *passwd, char *db)
{
	char connstr[255];
	int cpos = 0;
	
	if (conn->dblink != NULL)
		return 0;
		
	memset(connstr, 0, 255);

	if (host){
		if (isdigit(host[0])){
			sprintf(connstr, "hostaddr='%s' ", host);
		}else{
			sprintf(connstr, "host='%s' ", host);
		}
		cpos = strlen(connstr);
	}
	
	if (port){
		sprintf((char*)(connstr+cpos), "port=%i ", port);
		cpos = strlen(connstr);
	}
	
	if (user){
		sprintf((char*)(connstr+cpos), "user='%s' ", user);
		cpos = strlen(connstr);
	}
	
	if (passwd){
		sprintf((char*)(connstr+cpos), "password='%s' ", passwd);
		cpos = strlen(connstr);
	}
	
	if (db){
		sprintf((char*)(connstr+cpos), "dbname='%s' ", db);
		cpos = strlen(connstr);
	}

	conn->dblink = PQconnectdb(connstr);
	
	if ((PQstatus(conn->dblink) == CONNECTION_OK) || (PQstatus(conn->dblink) == CONNECTION_MADE)){
		return 1;
	}
	if (PQstatus(conn->dblink) == CONNECTION_BAD){
		return 0;
	}
	return 1;
}

int
plugin_conn_close (odkPGConnection * conn)
{
	if (conn->dblink == NULL)
		return 0;

	PQfinish (conn->dblink);
	return 1;
}

int
plugin_command_fetchex_into (odkCommand * com, int n, odkFieldDef *type, void *ptr)
{
	cmdData *d;
	odkValue **v;
	int i;
	char *tmp, buff[10];
	odkDate *da;
	odkTime *ti;
	int ptrpos, tmpi;
	odkFieldDef *defines;
	char *tmps;
#ifdef WIN32
	int sptr = (int)ptr;
#else
	void *sptr = ptr;
#endif
	
	d = (cmdData *) COMMAND (com).private_data;
		
	if (d == NULL)
		return 0;

	if (d->c >= PQntuples(d->res)){
		PQclear (d->res);
		COMMAND (com).private_data = NULL;
		free (d);
		return 0;
	}
	
	defines = type;
	ptrpos = 0;

	for (i = 0; i < COMMAND (com).colCount; i++) {
		
		tmp = PQgetvalue (d->res, d->c, i);
		switch (defines[i].type) {
		case ODKDA_TEXT:
			if (defines[i].flags | ODK_STRING_PTR){
				tmps = strcopy(tmp);
				*(char**)(sptr+ptrpos) = tmps;
				
				ptrpos+=sizeof(char*);
			}else{
				memset((void*)(sptr+ptrpos), 0, defines[i].size);
				if (PQgetlength (d->res,d->c, i) > defines[i].size){
					tmpi = defines[i].size;
				}else{
					tmpi = PQgetlength (d->res,d->c, i);
				}
				memcpy((void*)(sptr+ptrpos), tmp, tmpi);
				ptrpos += tmpi;
			}
			break;
		case ODKDA_INT:
			*(int*)(sptr+ptrpos) = atoi (tmp);
			ptrpos+=sizeof(int);
			break;
		case ODKDA_FLOAT:
			*(float*)(sptr+ptrpos) = atof(tmp);
			ptrpos+=sizeof(float);
			break;
		case ODKDA_DOUBLE:
			*(double*)(sptr+ptrpos) = atof(tmp);
			ptrpos+=sizeof(double);
			break;
		case ODKDA_BLOB:
			/*v[i]->size = PQgetlength (d->res,d->c, i);
			v[i]->vString = tmp;*/
			break;
		case ODKDA_DATE:
			da = (odkDate*)(sptr+ptrpos);
			
			memset (buff, 0, 10);
			memcpy (buff, tmp, 4);
			da->year = atoi(buff);
			
			memset (buff, 0, 10);
			memcpy (buff, (tmp + 5), 2);
			da->month = atoi(buff);
			
			memset (buff, 0, 10);
			memcpy (buff, (tmp + 8), 2);
			da->day = atoi(buff);
			
			ptrpos += sizeof(odkDate);
			break;
		case ODKDA_TIME:
			ti = (odkTime*)(sptr+ptrpos);
			
			memset (buff, 0, 10);
			memcpy (buff, tmp, 2);
			ti->hours = atoi(buff);
			
			memset (buff, 0, 10);
			memcpy (buff, (tmp + 3), 2);
			ti->minutes = atoi(buff);
			
			memset (buff, 0, 10);
			memcpy (buff, (tmp + 6), 2);
			ti->seconds = atoi(buff);
			
			ptrpos += sizeof(odkTime);
			break;
		}
	}

	d->c++;
	return 1;
}

odkValue **
plugin_command_fetch (odkCommand * com)
{
	cmdData *d;
	odkValue **v;
	int i;
	char *tmp, buff[10];
	odkDate *da;
	odkTime *ti;

	d = (cmdData *) COMMAND (com).private_data;

	if (d == NULL)
		return NULL;

	if (d->c >= PQntuples(d->res)){
		PQclear (d->res);
		COMMAND (com).private_data = NULL;
		free (d);
		return NULL;
	}

	v = (odkValue **) malloc (sizeof (odkValue *) * COMMAND (com).colCount);

	for (i = 0; i < COMMAND (com).colCount; i++) {
		v[i] = odk_value_new (COMMAND (com).columns[i]->type);
		tmp = PQgetvalue (d->res, d->c, i);
		switch (v[i]->type) {
		case ODKDA_TEXT:
			v[i]->size = PQgetlength (d->res,d->c, i);
			v[i]->vString = strcopy (tmp);
			break;
		case ODKDA_INT:
			v[i]->size = 4;
			v[i]->vInt = atoi(tmp);
			break;
		case ODKDA_FLOAT:
			v[i]->size = 4;
			v[i]->vFloat = atof(tmp);
			break;
		case ODKDA_DOUBLE:
			v[i]->size = 8;
			v[i]->vDouble = atof(tmp);
			break;
		case ODKDA_BLOB:
			v[i]->size = PQgetlength (d->res,d->c, i);
			v[i]->vString = tmp;
			break;
		case ODKDA_DATE:
			da = (odkDate*)malloc(sizeof(odkDate));
			
			memset (buff, 0, 10);
			memcpy (buff, tmp, 4);
			da->year = atoi(buff);
			
			memset (buff, 0, 10);
			memcpy (buff, (tmp + 5), 2);
			da->month = atoi(buff);
			
			memset (buff, 0, 10);
			memcpy (buff, (tmp + 8), 2);
			da->day = atoi(buff);
			
			v[i]->size = sizeof(odkDate);
			v[i]->vDate = da;
			break;
		case ODKDA_TIME:
			ti = (odkTime*)malloc(sizeof(odkTime));
			
			memset (buff, 0, 10);
			memcpy (buff, tmp, 2);
			ti->hours = atoi(buff);
			
			memset (buff, 0, 10);
			memcpy (buff, (tmp + 3), 2);
			ti->minutes = atoi(buff);
			
			memset (buff, 0, 10);
			memcpy (buff, (tmp + 6), 2);
			ti->seconds = atoi(buff);
			
			v[i]->size = sizeof(odkTime);
			v[i]->vTime = ti;
			break;
		}
	}

	d->c++;
	return v;
}

int
plugin_command_fetch_into (void *com, odkValue ** row)
{
	cmdData *d;
	odkValue **v;
	int i;
	char *tmp;

	d = (cmdData *) COMMAND (com).private_data;

	if (d == NULL)
		return 0;
	
	if (d->c >= PQntuples(d->res)){
		PQclear (d->res);
		COMMAND (com).private_data = NULL;
		free (d);
		return 0;
	}
	
	printf("Row: %i\nTuples: %s\n", d->c, PQntuples(d->res));

	v = row;

	for (i = 0; i < COMMAND (com).colCount; i++) {
		v[i] = odk_value_new (COMMAND (com).columns[i]->type);
		tmp = PQgetvalue (d->res, d->c, i);
		switch (v[i]->type) {
		case ODKDA_TEXT:
			v[i]->size = PQgetlength (d->res,d->c, i);
			v[i]->vString = strcopy (tmp);
			break;
		case ODKDA_INT:
			v[i]->size = 4;
			v[i]->vInt = atoi(tmp);
			break;
		case ODKDA_FLOAT:
			v[i]->size = 4;
			v[i]->vFloat = atof(tmp);
			break;
		case ODKDA_DOUBLE:
			v[i]->size = 8;
			v[i]->vDouble = atof(tmp);
			break;
		case ODKDA_BLOB:
			v[i]->size = PQgetlength (d->res,d->c, i);
			v[i]->vString = tmp;
			break;
		}
	}

	d->c++;
	return 1;
}

int
plugin_transaction_begin (odkPGConnection * conn)
{
	PGresult *res;
	res = PQexec(conn->dblink, "BEGIN TRANSACTION");

	if (res){
		PQclear (res);
		return 1;
	}

	return 0;
}

int
plugin_transaction_rollback (odkPGConnection * conn)
{
	PGresult *res;
	res = PQexec(conn->dblink, "ROLLBACK");

	if (res){
		PQclear (res);
		return 1;
	}

	return 0;
}

int
plugin_transaction_commit (odkPGConnection * conn)
{
	PGresult *res;
	
	if (res){
		PQclear (res);
		return 1;
	}

	return 0;
}

int
plugin_conn_free (odkPGConnection * conn)
{
	return false;
}

int
plugin_conn_exec_sql (odkPGConnection * conn, odkCommand * cmd)
{
	cmdData *d;
	int i;
	char *sql;
	odkLibHandle *l = DATAPROVIDER (CONNECTION (conn).parent).plugin;

	if (COMMAND (cmd).private_data != NULL)
		return 0;

	sql = odk_sql_prepare((odkSQLCommand*)cmd);
	
	if (sql == NULL)
		return 0;
	
	d = malloc (sizeof (cmdData));
	
	d->res = PQexec(conn->dblink, sql);
	
	free(sql);

	COMMAND (cmd).private_data = d;
	COMMAND (cmd).fetch =
		(FUNC_COM_FETCH ) odk_library_symbol (l, "plugin_command_fetch");
	COMMAND (cmd).fetchex_into =
		(FUNC_COM_FETCHEX_INTO ) odk_library_symbol (l, "plugin_command_fetchex_into");
	COMMAND (cmd).fetch_into =
		(FUNC_COM_FETCHINTO ) odk_library_symbol (l,
												   "plugin_command_fetch_into");
	COMMAND (cmd).colCount = 0;
	COMMAND (cmd).columns = NULL;

	if (PQntuples(d->res) == 0){
		PQclear(d->res);
		COMMAND (cmd).private_data = NULL;
		free(d);
		return 1;
	}

	COMMAND (cmd).colCount = PQnfields(d->res);
	COMMAND (cmd).rowCount = PQntuples(d->res);
	COMMAND (cmd).columns =
		malloc (sizeof (odkColumn *) * COMMAND (cmd).colCount);

	for (i = 0; i < COMMAND (cmd).colCount; i++) {
		COMMAND (cmd).columns[i] =
			odk_column_new ((char *) PQfname (d->res, i));
		COLUMN (COMMAND (cmd).columns[i]).type =
			pg2odk (PQftype(d->res, i));
	}

	COMMAND (cmd).affected = -1;
	d->c = 0;

	return 1;
}

int
plugin_conn_exec (odkPGConnection * conn, odkCommand * cmd)
{
	if (COMMAND (cmd).type == 1)
		return plugin_conn_exec_sql (conn, cmd);

	return 0;
}

int
plugin_provider_init (odkDataProvider *dp)
{
	odkBitfield features;
	odkBitfield sqlfeatures;
	odkSList *extensions;
	
	features = DATAPROVIDER(dp).features;
	sqlfeatures = DATAPROVIDER(dp).sqlfeatures;
	extensions = DATAPROVIDER(dp).extensions;
	
	odk_bitfield_set(features, ODKDA_SQL);
	odk_bitfield_set(features, ODKDA_TRANSACTIONS);	
	odk_bitfield_set(features, ODKDA_ROWCOUNT);

	odk_slist_append(extensions, strcopy("ext_table_list"));
	
	return 1;
}

odkPGConnection *
plugin_connection_new ()
{
	odkPGConnection *conn =
		(odkPGConnection *) malloc (sizeof (odkPGConnection));

	object_register (conn, "odkPGConnection");
	object_append_destructor (conn, (FREE_FUNC)&plugin_conn_free);

	conn->dblink = NULL;

	return conn;
}
