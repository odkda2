/*
ODKDA: Data Access Library
        Oracle Plugin
		
Copyright (C) 2006 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>

#include <odkutils.h>
#include <odkutils_interop.h>
#include <odkutils_xml.h>

#include <odkda2.h>
#include <oci.h>

static OCIStmt          *p_sql;
static OCIDefine        *p_dfn    = (OCIDefine *) 0;
static OCIBind          *p_bnd    = (OCIBind *) 0;

typedef struct {
	CLASS_CONNECTION 
	PGconn *dblink;
	OCIEnv           *p_env;
	OCIError         *p_err;
	OCISvcCtx        *p_svc;
} odkOracleConnection;

typedef struct {
	OCIStmt *stmt;
	OCIDefine *defs;
	int c;
} cmdData;

int
pg2odk (int d)
{
	switch (d) {
	case INT2OID:
	case INT4OID:
		return ODKDA_INT;
	case INT8OID:
		return ODKDA_LONG;
	case FLOAT4OID:
		return ODKDA_FLOAT;
	case FLOAT8OID:
		return ODKDA_DOUBLE;
	case VARCHAROID:
		return ODKDA_TEXT;
	default:
		return 0;
	}
}

int
plugin_conn_open (odkOracleConnection * conn, char *host, int port,
				  char *user, char *passwd, char *db)
{
	int ret;
	OCIEnvCreate(&environment, OCI_DEFAULT, NULL, malloc, realloc, free, 0, NULL);
	//ret = OCIEnvInit( (OCIEnv **) &(conn->p_env), OCI_DEFAULT, (size_t) 0, (dvoid **) 0 );
	ret = OCIHandleAlloc( (dvoid *) conn->p_env, (dvoid **) &(con->p_err), OCI_HTYPE_ERROR,
           (size_t) 0, (dvoid **) 0);
	ret = OCIHandleAlloc( (dvoid *) conn->p_env, (dvoid **) &(conn->p_svc), OCI_HTYPE_SVCCTX,
           (size_t) 0, (dvoid **) 0);

	ret = OCILogon(conn->p_env, conn->p_err, &conn->p_svc, user, stelen(user), passwd, strlen(passwd), db, strlen(db));
	if (ret != 0) {
		//OCIErrorGet((dvoid *)p_err, (ub4) 1, (text *) NULL, &errcode, errbuf, (ub4) sizeof(errbuf), OCI_HTYPE_ERROR);
		//printf("Error - %.*s\n", 512, errbuf);
		//exit(8);
	}
   
	return !ret;
}

int
plugin_conn_close (odkPGConnection * conn)
{
	ret = OCILogoff(conn->p_svc, conn->p_err);

	return 1;
}

odkValue **
plugin_command_fetch (odkCommand * com)
{
	cmdData *d;
	odkValue **v;
	int i;
	char *tmp;

	d = (cmdData *) COMMAND (com).private_data;

	if (d == NULL)
		return NULL;

	if (d->c >= PQntuples(d->res)){
		PQclear (d->res);
		COMMAND (com).private_data = NULL;
		free (d);
		return NULL;
	}

	v = (odkValue **) malloc (sizeof (odkValue *) * COMMAND (com).colCount);

	for (i = 0; i < COMMAND (com).colCount; i++) {
		v[i] = odk_value_new (COMMAND (com).columns[i]->type);
		tmp = PQgetvalue (d->res, d->c, i);
		switch (v[i]->type) {
		case ODKDA_TEXT:
			v[i]->size = PQgetlength (d->res,d->c, i);
			v[i]->vString = strcopy (tmp);
			break;
		case ODKDA_INT:
			v[i]->size = 4;
			v[i]->vInt = atoi(tmp);
			break;
		case ODKDA_FLOAT:
			v[i]->size = 4;
			v[i]->vFloat = atof(tmp);
			break;
		case ODKDA_DOUBLE:
			v[i]->size = 8;
			v[i]->vDouble = atof(tmp);
			break;
		case ODKDA_BLOB:
			v[i]->size = PQgetlength (d->res,d->c, i);
			v[i]->vString = tmp;
			break;
		}
	}

	d->c++;
	return v;
}

int
plugin_command_fetch_into (void *com, odkValue ** row)
{
	cmdData *d;
	odkValue **v;
	int i;
	char *tmp;

	d = (cmdData *) COMMAND (com).private_data;

	if (d == NULL)
		return 0;
	
	if (d->c >= PQntuples(d->res)){
		PQclear (d->res);
		COMMAND (com).private_data = NULL;
		free (d);
		return 0;
	}
	
	printf("Row: %i\nTuples: %s\n", d->c, PQntuples(d->res));

	v = row;

	for (i = 0; i < COMMAND (com).colCount; i++) {
		v[i] = odk_value_new (COMMAND (com).columns[i]->type);
		tmp = PQgetvalue (d->res, d->c, i);
		switch (v[i]->type) {
		case ODKDA_TEXT:
			v[i]->size = PQgetlength (d->res,d->c, i);
			v[i]->vString = strcopy (tmp);
			break;
		case ODKDA_INT:
			v[i]->size = 4;
			v[i]->vInt = atoi(tmp);
			break;
		case ODKDA_FLOAT:
			v[i]->size = 4;
			v[i]->vFloat = atof(tmp);
			break;
		case ODKDA_DOUBLE:
			v[i]->size = 8;
			v[i]->vDouble = atof(tmp);
			break;
		case ODKDA_BLOB:
			v[i]->size = PQgetlength (d->res,d->c, i);
			v[i]->vString = tmp;
			break;
		}
	}

	d->c++;
	return 1;
}

int
plugin_transaction_begin (odkPGConnection * conn)
{
	PGresult *res;
	res = PQexec(conn->dblink, "BEGIN TRANSACTION");

	if (res){
		PQclear (res);
		return 1;
	}

	return 0;
}

int
plugin_transaction_rollback (odkPGConnection * conn)
{
	PGresult *res;
	res = PQexec(conn->dblink, "ROLLBACK");

	if (res){
		PQclear (res);
		return 1;
	}

	return 0;
}

int
plugin_transaction_commit (odkPGConnection * conn)
{
	PGresult *res;
	res = PQexec(conn->dblink, "COMMIT");

	if (res){
		PQclear (res);
		return 1;
	}

	return 0;
}

int
plugin_conn_free (odkPGConnection * conn)
{
	return false;
}

int
plugin_conn_exec_sql (odkOracleConnection * conn, odkCommand * cmd)
{
	cmdData *d;
	int i, ret;
	char *sql;
	LIBHANDLE *l = DATAPROVIDER (CONNECTION (conn).parent).plugin;

	if (COMMAND (cmd).private_data != NULL)
		return 0;

	sql = odk_sql_prepare((odkSQLCommand*)cmd);
	
	if (sql == NULL)
		return 0;
		
 	ret = OCIHandleAlloc( (dvoid *) conn->p_env, (dvoid **) &(d->stmt),
           OCI_HTYPE_STMT, (size_t) 0, (dvoid **) 0);
	
	d = malloc (sizeof (cmdData));
	
	d->res = PQexec(conn->dblink, sql);
	
	free(sql);

	COMMAND (cmd).private_data = d;
	COMMAND (cmd).fetch =
		(FUNC_COM_FETCH ) odk_library_symbol (l, "plugin_command_fetch");
	COMMAND (cmd).fetch_into =
		(FUNC_COM_FETCHINTO ) odk_library_symbol (l,
												   "plugin_command_fetch_into");
	COMMAND (cmd).colCount = 0;
	COMMAND (cmd).columns = NULL;

	if (PQntuples(d->res) == 0){
		PQclear(d->res);
		COMMAND (cmd).private_data = NULL;
		free(d);
		return 1;
	}

	COMMAND (cmd).colCount = PQnfields(d->res);
	COMMAND (cmd).rowCount = PQntuples(d->res);
	COMMAND (cmd).columns =
		malloc (sizeof (odkColumn *) * COMMAND (cmd).colCount);

	for (i = 0; i < COMMAND (cmd).colCount; i++) {
		COMMAND (cmd).columns[i] =
			odk_column_new ((char *) PQfname (d->res, i));
		COLUMN (COMMAND (cmd).columns[i]).type =
			pg2odk (PQftype(d->res, i));
	}

	COMMAND (cmd).affected = -1;
	d->c = 0;

	return 1;
}

int
plugin_conn_exec (odkPGConnection * conn, odkCommand * cmd)
{
	if (COMMAND (cmd).type == 1)
		return plugin_conn_exec_sql (conn, cmd);

	return 0;
}

int
plugin_provider_init (odkDataProvider *dp)
{
	odkBitfield features;
	odkBitfield sqlfeatures;
	
	OCIInitialize((ub4) OCI_DEFAULT, (dvoid *)0,
           (dvoid * (*)(dvoid *, size_t)) 0,
           (dvoid * (*)(dvoid *, dvoid *, size_t))0,
           (void (*)(dvoid *, dvoid *)) 0 );

	features = DATAPROVIDER(dp).features;
	sqlfeatures = DATAPROVIDER(dp).sqlfeatures;

	odk_bitfield_set(features, ODKDA_SQL);
	odk_bitfield_set(features, ODKDA_TRANSACTIONS);	
	odk_bitfield_set(features, ODKDA_ROWCOUNT);

	return 1;
}

odkPGConnection *
plugin_connection_new ()
{
	odkPGConnection *conn =
		(odkPGConnection *) malloc (sizeof (odkPGConnection));

	object_register (conn, "odkPGConnection");
	object_append_destructor (conn, (FREE_FUNC)&plugin_conn_free);

	conn->dblink = NULL;

	return conn;
}
