/*
ODKDA: Data Access Library
        MySQL Plugin
		
Copyright (C) 2006 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>

#include <odkutils.h>
#include <odkutils_interop.h>
#include <odkutils_xml.h>

#include <odkda2.h>
#include <ctype.h>
#include <ibase.h>


typedef struct {
	CLASS_CONNECTION 
	isc_db_handle dblink;
	ISC_STATUS_ARRAY stats;
	isc_tr_handle trans;
} odkFirebirdConnection;

typedef struct{
	isc_stmt_handle stmt;
} odkFirebirdData;

int
enum2odk (int d)
{
	switch (d) {
	case SQL_TEXT:
	case SQL_VARING:
		return ODKDA_TEXT;
	case FIELD_TYPE_BLOB:
		return ODKDA_BLOB;
	case SQL_FLOAT:
		return ODKDA_FLOAT;
	case SQL_DOUBLE:
		return ODKDA_DOUBLE;
	case FIELD_TYPE_TINY:
		return ODKDA_CHAR;
	case SQL_SHORT:
		return ODKDA_SHORT;
	case SQL_LONG:
	case FIELD_TYPE_LONG:
		return ODKDA_INT;
	case FIELD_TYPE_LONGLONG:
		return ODKDA_LONG;
	}
	return 0;
}


int
plugin_conn_open (odkFirebirdConnection * conn, char *host, int port,
				  char *user, char *passwd, char *db)
{
	if (isc_attach_database(conn->status, 0, db, &(conn->dblink), 0, NULL)){
		return 0;
	}

	return 1;
}

int
plugin_conn_close (odkFirebirdConnection * conn)
{
	MYSQL *dblink = conn->dblink;

	if (dblink == NULL)
		return 0;

	if (isc_detach_database(conn->status, &(conn->dblink))){
		return 0;
	}

	conn->dblink = NULL;
	return 1;
}

int
plugin_command_free (odkCommand * cmd)
{
	return true;
}

odkValue **
plugin_command_fetch (odkCommand * com)
{
	MYSQL_RES *res;
	MYSQL_ROW row;
	odkValue **v;
	int i;
	unsigned long *lengths;
	char tmp[256];

	res = (MYSQL_RES *) COMMAND (com).private_data;

	if (res == NULL)
		return NULL;

	if (!(row = mysql_fetch_row (res))) {
		mysql_free_result (res);
		COMMAND (com).private_data = NULL;
		return NULL;
	}

	v = malloc (sizeof (odkValue *) * COMMAND (com).colCount);

	lengths = mysql_fetch_lengths (res);
	for (i = 0; i < COMMAND (com).colCount; i++) {		
		v[i] = odk_value_new (COMMAND (com).columns[i]->type);
		switch (v[i]->type) {
		case ODKDA_TEXT:
			v[i]->size = lengths[i];
			v[i]->vString = malloc (lengths[i] + 1);
			memset (v[i]->vString, 0, lengths[i] + 1);
			memcpy (v[i]->vString, row[i], lengths[i]);
			break;
		case ODKDA_INT:
			memset (tmp, 0, 256);
			memcpy (tmp, row[i], lengths[i]);
			v[i]->vInt = atoi (tmp);
			break;
		case ODKDA_FLOAT:
			memset (tmp, 0, 256);
			memcpy (tmp, row[i], lengths[i]);
			v[i]->vFloat = (float) atof (tmp);
			break;
		case ODKDA_DOUBLE:
			memset (tmp, 0, 256);
			memcpy (tmp, row[i], lengths[i]);
			v[i]->vDouble = strtod (tmp, NULL);
			break;
		case ODKDA_BLOB:
			v[i]->size = lengths[i];
			v[i]->vBlob = malloc (lengths[i] + 1);
			memcpy (v[i]->vBlob, row[i], lengths[i]);
			break;
		}
	}

	return v;

}

int
plugin_command_fetch_into (void *com, odkValue ** row)
{
	return false;
}

int
plugin_conn_free (odkFirebirdConnection * conn)
{
	return true;
}

int
plugin_conn_exec_sql (odkFirebirdConnection * conn, odkCommand * cmd)
{
	XSQLDA *res;
	odkDataProvider *dp = CONNECTION (conn).parent;
	int i;
	LIBHANDLE lib = DATAPROVIDER (dp).plugin;

	if (COMMAND (cmd).private_data != NULL)
		return 0;

	sqlda = (XSQLDA *) malloc(XSQLDA_LENGTH(3));
    sqlda->sqln = 3;
    sqlda->version = 1;
	
	if (mysql_query (dblink, SQLCOMMAND (cmd).sql) != 0)
		return 0;

	if (mysql_field_count(dblink) == 0)
		return 1;
	
	res = mysql_store_result (dblink);
	myfields = mysql_fetch_fields (res);

	COMMAND (cmd).private_data = res;
	COMMAND (cmd).colCount = mysql_num_fields (res);
	COMMAND (cmd).rowCount = mysql_num_rows (res);
	COMMAND (cmd).fetch =
		(FUNC_COM_FETCH *) odk_library_symbol (lib, "plugin_command_fetch");
	COMMAND (cmd).fetch_into =
		(FUNC_COM_FETCHINTO *) odk_library_symbol (lib, "plugin_command_fetch_into");
	COMMAND (cmd).affected = mysql_affected_rows (dblink);

	if (res == NULL) {
		if (mysql_field_count (dblink) == 0) {
			COMMAND (cmd).private_data = NULL;
			COMMAND (cmd).columns = NULL;
			return 1;
		}
	}

	COMMAND (cmd).columns =
		malloc (sizeof (odkColumn *) * COMMAND (cmd).colCount);

	for (i = 0; i < COMMAND (cmd).colCount; i++) {
		COMMAND (cmd).columns[i] = odk_column_new (myfields[i].name);
		COLUMN (COMMAND (cmd).columns[i]).type = enum2odk (myfields[i].type);
	}

	return 1;
}

int
plugin_conn_exec (odkFirebirdConnection * conn, odkCommand * cmd)
{
	if (COMMAND (cmd).type == 1)
		return plugin_conn_exec_sql (conn, cmd);

	return -1;
}

int 
plugin_transaction_begin (odkFirebirdConnection * conn)
{
	if (!conn->trans){
		if (isc_start_transaction(status, &(conn->trans), 1, &(conn->db), 0, NULL)){
			return 0;
		}else{
			return 1;
		}
	}
	return 0;
}

int 
plugin_transaction_rollback (odkMySQLConnection * conn)
{
	if (!conn->trans){
		if (isc_rollback_transaction(conn->status, &(conn->trans))){
			return 0;
		}else{
			return 1;
		}
	}
	return 0;
}

int 
plugin_transaction_commit (odkMySQLConnection * conn)
{
	if (!conn->trans){
		if (isc_commit_transaction(conn->status, &(conn->trans))){
			return 0;
		}else{
			return 1;
		}
	}
	return 0;
}

int
plugin_provider_init (odkDataProvider *dp)
{
	odkBitfield features;
	odkBitfield sqlfeatures;
	odkSList *extensions;

	features = DATAPROVIDER(dp).features;
	sqlfeatures = DATAPROVIDER(dp).sqlfeatures;
	extensions = DATAPROVIDER(dp).extensions;

	odk_bitfield_set(features, ODKDA_SQL);
	odk_bitfield_set(features, ODKDA_TRANSACTIONS);
	odk_bitfield_set(features, ODKDA_ROWCOUNT);

	return 1;
}

odkFirebirdConnection *
plugin_connection_new ()
{
	odkFirebirdConnection *conn =
		(odkFirebirdConnection *) malloc (sizeof (odkFirebirdConnection));

	object_register (conn, "odkFirebirdConnection");
	object_append_destructor (conn, (FREE_FUNC)&plugin_conn_free);

	conn->dblink = NULL;
	conn->stats = NULL;
	conn->trans = NULL;

	return conn;
}
