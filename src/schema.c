/*
ODKDA: Data Access Library
        Schema Object
		
Copyright (C) 2006 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdio.h>
#include <malloc.h>
#include <string.h>

#include <odkutils.h>

#include <odkda/common.h>
#include <odkda/column.h>
#include <odkda/date.h>
#include <odkda/time.h>
#include <odkda/datetime.h>
#include <odkda/value.h>
#include <odkda/schema.h>

void
odk_schema_del (odkSchema * schm)
{
	int i;
	odkColumn *c;

	for_list(SCHEMA (schm).columns){
		c = (odkColumn*)odk_list_data(SCHEMA (schm).columns);
		object_unref (c);
	}

	free (SCHEMA (schm).name);
	free (SCHEMA (schm).columns);
}

odkSchema *
odk_schema_new (char *name)
{
	odkSchema *schm;

	schm = (odkSchema *) malloc (sizeof (odkSchema));

	object_register (schm, "odkSchema");
	object_append_destructor (schm, (FREE_FUNC)&odk_schema_del);

	SCHEMA (schm).name = strcopy(name);
	SCHEMA (schm).columns = odk_list_new(0);

	SCHEMA (schm).key = NULL;
	SCHEMA (schm).defaults = NULL;

	return schm;
}

int
odk_schema_colcount (odkSchema * schm)
{
	if (schm == NULL)
		return 0;

	return odk_list_count(SCHEMA (schm).columns);
}

odkList *
odk_schema_columns (odkSchema * schm)
{
	if (schm == NULL)
		return NULL;

	return SCHEMA (schm).columns;
}

odkColumn *
odk_schema_get_column (odkSchema * schm, int n)
{
	if (schm == NULL)
		return NULL;

	if (!odk_list_nth(SCHEMA (schm).columns, n))
		return NULL;

	return (odkColumn*)odk_list_data(SCHEMA (schm).columns);
}

odkColumn *
odk_schema_get_key (odkSchema * schm)
{
	if (schm == NULL)
		return NULL;

	return SCHEMA (schm).key;
}
