/*
ODKDA: Data Access Library
        Select Command Object
		
Copyright (C) 2006 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdio.h>
#include <malloc.h>
#include <string.h>

#define NOMACROS
#include <odkutils.h>
#include <odkutils_xml.h>
#include <odkutils_interop.h>

#include <odkda/common.h>
#include <odkda/column.h>
#include <odkda/date.h>
#include <odkda/time.h>
#include <odkda/datetime.h>
#include <odkda/value.h>
#include <odkda/command.h>
#include <odkda/selectcmd.h>

void order_column_del(odkOrderColumn *c){
	free(c->column);
	free(c);
}

void odk_select_delete(odkSelectCommand *c){

}

odkSelectCommand *odk_select_new(char *table){
	odkSelectCommand *c = (odkSelectCommand *) malloc (sizeof (odkSelectCommand));

	object_register (c, "odkSelectCommand");
	object_append_destructor (c, (FREE_FUNC)&odk_select_delete);
	
	c->table = table;
	c->columns = odk_slist_new(0);
	c->orders = odk_slist_new(0);
	c->limit = 0;
	c->offset = 0;
	
	return c;
}

odkSelectCommand *odk_select_new1(){
	odkSelectCommand *c = (odkSelectCommand *) malloc (sizeof (odkSelectCommand));

	object_register (c, "odkSelectCommand");
	object_append_destructor (c, (FREE_FUNC)&odk_select_delete);
	
	c->table = NULL;
	c->columns = odk_slist_new(0);
	c->orders = odk_slist_new(0);
	c->limit = 0;
	c->offset = 0;
	
	return c;
}

int odk_select_add_column(odkSelectCommand *c, char *column, char *table){
	if (c == NULL)
		return 0;
	
	odk_slist_append(c->columns, strcopy(column));
}

int odk_select_add_order(odkSelectCommand *c, char *column, char order){
	odkOrderColumn *oc;
	
	if (c == NULL)
		return 0;
	
	oc = malloc(sizeof(odkOrderColumn));
	oc->column = strcopy(column);
	oc->order = order;
	
	return 1;
}

int odk_select_set_limit(odkSelectCommand *c, int limit){
	if (c == NULL)
		return 0;
	
	c->limit = limit;
	return 1;
}

char *odk_select_tosql(odkSelectCommand *c){
	int s, d;
	char *tmp;
	char *sql;
	
	if (c == NULL)
		return NULL;
	
	s = 0;
	for_slist(c->columns){
		tmp = odk_slist_data(c->columns);
		s += strlen(tmp);
	}
	
	s += 100;
	
	sql = (char*)malloc(s);
	memset(sql, 0, s);
	
	sprintf(sql, "SELECT ");
	
	s= 7;
	d = 0;
	for_slist(c->columns){
		tmp = odk_slist_data(c->columns);
		d = strlen(tmp);
		memcpy((char*)(sql+s), tmp, d);
		s+=d;
		if ((odk_slist_index(c->columns)+1) != odk_slist_count(c->columns)){
			sprintf((char*)(sql+s), ", ");
			s+=2;
		}
	}
	
	sprintf((char*)(sql+s), " FROM %s", c->table);
	
	return sql;
}
