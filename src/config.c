#include <stdio.h>
#include <string.h>
#include <malloc.h>

#include <odkutils.h>
#include <odkutils_xml.h>

#ifdef _HAVE_CONFIG_
#include "config.h"
#else
#define VERSION="0.5.2"
#define PREFIX="/usr"
#define ETCPATH="/etc"
#define CONFFILE= "/etc/odkda.conf"
#endif

char cflags, libs;

void
register_local (char *file)
{
	odkXMLNode *r, *c, *tmp;
	odkSList *l;

	r = odk_xml_parse (file);

	if (r == NULL)
		return;

	c = odk_xml_parse ("odkda.conf");

	if (c == NULL) {
		c = odk_xml_node_new (NULL, "configuration");
		odk_xml_attrib_set (c, "type", "dir");
	}

	for_slist (c->children) {
		tmp = odk_slist_data (c->children);
		if (!strcmp (odk_xml_name (r), odk_xml_name (tmp))) {
			odk_slist_delete (c->children);
			odk_xml_free (tmp);
		}
	}

	odk_xml_reparent (c, r);
	odk_xml_write ("odkda.conf", c);

	odk_xml_free (c);
}

void
register_plugin (char *file)
{
	odkXMLNode *r, *c, *tmp;
	odkSList *l;
	odkLibHandle *h;
	char buff[100];
	char cmd[400];
	char *lib;

	r = odk_xml_parse (file);

	if (r == NULL)
		return;

	lib = strcopy (odk_xml_data_get (odk_xml_child_get (r, "lib")));

	if (lib == NULL)
		return;

	sprintf (buff, "%.so", lib);
	c = odk_xml_parse (CONFFILE);

	if (c == NULL) {
		c = odk_xml_node_new (NULL, "configuration");
		odk_xml_attrib_set (c, "type", "dir");
	}

	for_slist (c->children) {
		tmp = odk_slist_data (c->children);
		if (!strcmp (odk_xml_name (r), odk_xml_name (tmp))) {
			odk_slist_delete (c->children);
			odk_xml_free (tmp);
		}
	}

	odk_xml_reparent (c, r);
	odk_xml_write (CONFFILE, c);

	odk_xml_free (c);

	if (strstr (lib, "&")) {
		printf ("Invalid library %s\n");
		return;
	}

	if (strstr (lib, ";")) {
		printf ("Invalid library %s\n");
		return;
	}

	if (strstr (lib, " ")) {
		printf ("Invalid library %s\n");
		return;
	}

	free (lib);
}

void
plugins_info ()
{
	odkXMLNode *r;
	odkXMLNode *c;

	r = odk_xml_parse (CONFFILE);

	if (r == NULL) {
		printf
			("Error: no configuration file found, check installation or register plugins\n");
		return;
	}

	if (strcmp (odk_xml_name (r), "configuration")) {
		printf ("Error: corrupted configuration file, check installation\n");
	}

	for_slist (r->children) {
		c = odk_slist_data (r->children);
		printf ("%s:\n", odk_xml_name (c));
		printf ("  Author: %s\n", odk_xml_path_data_get (c, "/meta/Author"));
		printf ("  License: %s\n", odk_xml_path_data_get (c, "/meta/License"));
		printf ("  Library: %s\n", odk_xml_path_data_get (c, "/lib"));
	}

	odk_xml_free (r);
}

void
version ()
{
	printf ("%s\n", VERSION);
}

void
usage ()
{
	printf ("ODK Data Access Config Script\n");
	printf ("Usage:\n");
	printf ("odkda-config [--libs] [--cflags] [--register plugin.xml]\n");
	printf ("\nAditional Options:\n");
	printf ("  --version  Print library version\n");
	printf ("  --libs     Print library linking information\n");
	printf ("  --cflags   Print compilation flags information\n");
	printf ("  --version  Print ODK Version\n");
	printf ("  --plugins  Print available plugins\n");
	printf ("  --register Register a plugin (system wide)\n");
	printf ("             should be used with NO other options\n");
	printf ("  --rlocal   Register a plugin locally\n");
	printf ("             in odkda.conf in current directory.\n");
}

int
main (int argc, char **argv)
{
	int i;
	cflags = 0;
	libs = 0;

	if (argc == 1) {
		usage ();
		return 0;
	}

	if (!strcmp (argv[1], "--register")) {
		if (argc < 3) {
			usage ();
		} else {
			register_plugin (argv[2]);
		}
		return 0;
	}

	if (!strcmp (argv[1], "--rlocal")) {
		if (argc < 3) {
			usage ();
		} else {
			register_local (argv[2]);
		}
		return 0;
	}

	for (i = 1; i < argc; i++) {
		if (!strcmp (argv[i], "--plugins")) {
			plugins_info ();
			return 0;
		} else if (!strcmp (argv[i], "--version")) {
			version ();
			return 0;
		} else if (!strcmp (argv[i], "--libs")) {
			libs = 1;
		} else if (!strcmp (argv[i], "--cflags")) {
			cflags = 1;
		} else {
			usage ();
			return 0;
		}
	}

	if (libs)
		printf (" -lodkda2 ");

	if (cflags)
		printf (" -L%s -I%s ", LIBPATH, INCPATH);

	return 0;
}
