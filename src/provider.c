/*
ODKDA: Data Access Library
        Data Provider Object
		
Copyright (C) 2006 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdio.h>
#include <malloc.h>
#include <string.h>

#include <odkutils.h>
#include <odkutils_xml.h>

#include <odkda/common.h>
#include <odkda/date.h>
#include <odkda/time.h>
#include <odkda/datetime.h>
#include <odkda/column.h>
#include <odkda/value.h>
#include <odkda/command.h>
#include <odkda/provider.h>
#include <odkda/connection.h>

#ifdef _HAVE_CONFIG_
#include <config.h>
#else
#ifdef WIN32
char *configs[2] = { "odkda.conf", "c:\\windows\\odkda.conf" };
#else
char *configs[2] = { "/etc/odkda2/odkda.conf", "/etc/odkda.conf", "odkda.conf" };
#endif
#endif

odkConfigStore *
odk_load_config ()
{
	odkConfigStore *cs;
	int i;

	for (i = 0; i < 2; i++) {
		cs = (odkConfigStore *) odk_xmlconfig_new (configs[i], "config");

		if (!odk_config_load (cs)) {
			object_unref (cs);
			cs = NULL;
		} else {
			break;
		}
	}

	return cs;
}

void
odk_dataprovider_delete (odkDataProvider * dp)
{
	odk_slist_free (DATAPROVIDER (dp).extensions, &free);
	odk_library_close (DATAPROVIDER (dp).plugin);
}

odkSList *
odk_dataprovider_enum ()
{
	odkConfigStore *cs;
	odkSList *l;

	cs = odk_load_config ();

	if (cs == NULL)
		return NULL;

	l = odk_config_key_list (cs, "/");
	object_unref (cs);

	return l;
}

odkDataProvider *
odk_dataprovider_new (char *provider)
{
	odkConfigStore *cs;
	odkDataProvider *dp;
	char *lib;
	char buff[300];
	FUNC_INIT dp_init;

	cs = odk_load_config ();

	if (cs == NULL) {
		return NULL;
	}

	sprintf (buff, "/%s/lib", provider);
	if (odk_config_key_type (cs, buff) == -1) {
		object_unref (cs);
		return NULL;
	}

	lib = odk_config_key_get (cs, buff);

	if (lib == NULL) {
		object_unref (cs);
		return NULL;
	}

	dp = (odkDataProvider *) malloc (sizeof (odkDataProvider));
	object_register (dp, "odkDataProvider");
	object_append_destructor (dp, (FREE_FUNC)&odk_dataprovider_delete);

	DATAPROVIDER (dp).plugin = odk_library_open (lib);
	
	if (DATAPROVIDER (dp).plugin == NULL){
		return NULL;
	}
	

	DATAPROVIDER (dp).conn_new = (FUNC_CONNECTION_NEW)odk_library_symbol (DATAPROVIDER (dp).plugin, "plugin_connection_new");
	dp_init = (FUNC_INIT)odk_library_symbol (DATAPROVIDER (dp).plugin, "plugin_provider_init");

	if (DATAPROVIDER (dp).conn_new == NULL)  {
		return NULL;
	}
	
	if (dp_init == NULL){
		return NULL;
	}

	DATAPROVIDER (dp).features = odk_bitfield_new(ODKDA_FEATURE_BITCOUNT);
	DATAPROVIDER (dp).sqlfeatures = odk_bitfield_new(ODKDA_SQLFEATURE_BITCOUNT);
	DATAPROVIDER (dp).extensions = odk_slist_new(0);

	dp_init (dp);

	object_unref (cs);
	return dp;
}

int
odk_dataprovider_supports (odkDataProvider * dp, int feature)
{
	if (dp == NULL)
		return 0;

	return odk_bitfield_get(DATAPROVIDER (dp).features, feature);
}

int
odk_dataprovider_sql_supports (odkDataProvider * dp, int feature)
{
	if (dp == NULL)
		return 0;

	return odk_bitfield_get(DATAPROVIDER (dp).sqlfeatures, feature);
}

odkSList *
odk_dataprovider_extlist (odkDataProvider * dp)
{
	if (dp == NULL)
		return NULL;

	return DATAPROVIDER (dp).extensions;
}

bool odk_dataprovider_has_extension(odkDataProvider * dp, char *ext){
	char *e;

	if (dp == NULL)
		return false;

	if (ext == NULL)
		return false;

	for_slist (DATAPROVIDER (dp).extensions) {
		e = odk_slist_data (DATAPROVIDER (dp).extensions);

		if (!strcmp (e, ext)) {
			return true;
		}
	}

	return false;
}

void *
odk_dataprovider_extension (odkDataProvider * dp, char *ext)
{
	char *e;
	void *ptr;

	if (dp == NULL)
		return NULL;

	if (ext == NULL)
		return NULL;

	for_slist (DATAPROVIDER (dp).extensions) {
		e = odk_slist_data (DATAPROVIDER (dp).extensions);

		if (!strcmp (e, ext)) {
			ptr = odk_library_symbol (DATAPROVIDER (dp).plugin, ext);

			return ptr;
		}
	}

	return NULL;
}
