/*
ODKDA: Data Access Library
        Value Object
		
Copyright (C) 2006 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>

#include <odkutils.h>
#include <odkutils_xml.h>

#include <odkda/common.h>
#include <odkda/date.h>
#include <odkda/time.h>
#include <odkda/datetime.h>
#include <odkda/value.h>

void
odk_value_delete (odkValue * v)
{
	switch (v->type) {
	case ODKDA_TEXT:
		free (v->vString);
		break;
	case ODKDA_BLOB:
		free (v->vBlob);
		break;
	case ODKDA_DATE:
		free (v->vDate);
		break;
	case ODKDA_TIME:
		free (v->vTime);
		break;
	case ODKDA_DATETIME:
		free (v->vDateTime);
		break;
	default:
		break;
	}
}

void
odk_value_init (odkValue * v)
{
	v->changed = 0;
	switch (v->type) {
	case ODKDA_TEXT:
		v->vString = NULL;
		break;
	case ODKDA_BLOB:
		v->vBlob = NULL;
		break;
	case ODKDA_DATE:
		v->vDate = NULL;
		break;
	case ODKDA_TIME:
		v->vTime = NULL;
		break;
	case ODKDA_DATETIME:
		v->vDateTime = NULL;
		break;
	case ODKDA_BOOL:
		v->vBool = false;
		break;
	case ODKDA_CHAR:
		v->vChar = 0;
		break;
	case ODKDA_SHORT:
		v->vShort = 0;
		break;
	case ODKDA_INT:
		v->vInt = 0;
		break;
	case ODKDA_LONG:
		v->vLong = 0;
		break;
	case ODKDA_FLOAT:
		v->vFloat = 0.0;
		break;
	case ODKDA_DOUBLE:
		v->vDouble = 0.0;
		break;
	default:
		break;
	}
}

odkValue *
odk_value_new (char type)
{
	odkValue *v = (odkValue *) malloc (sizeof (odkValue));

	object_register (v, "odkValue");
	object_append_destructor (v, (FREE_FUNC)&odk_value_delete);
	v->type = type;
	v->size = 0;

	if (type != 0)
		odk_value_init (v);
	return v;
}

odkValue *
odk_value_new_shared (char type, odkMemPool * m)
{
	odkValue *v = (odkValue *) odk_mempool_alloc (m, sizeof (odkValue));

	object_register (v, "odkValue");
	object_append_destructor (v, (FREE_FUNC)&odk_value_delete);
	v->type = type;
	v->size = 0;

	if (type != 0)
		odk_value_init (v);
	return v;
}

int
odk_value_set (odkValue * v, char *val)
{
	if (v == NULL)
		return 0;

	v->changed = 1;
	v->vString = strcopy (val);
	v->size = strlen(val);
}

int
odk_value_set_bool (odkValue * v, bool val)
{
	if (v == NULL)
		return 0;

	v->changed = 1;
	v->vBool = val;
}

int
odk_value_set_char (odkValue * v, char val)
{
	if (v == NULL)
		return 0;

	v->changed = 1;
	v->vChar = val;
}

int
odk_value_set_short (odkValue * v, short val)
{
	if (v == NULL)
		return 0;

	v->changed = 1;
	v->vShort = val;
}

int
odk_value_set_int (odkValue * v, int val)
{
	if (v == NULL)
		return 0;

	v->changed = 1;
	v->vInt = val;
}

int
odk_value_set_long (odkValue * v, long val)
{
	if (v == NULL)
		return 0;

	v->changed = 1;
	v->vLong = val;
}

int
odk_value_set_float (odkValue * v, float val)
{
	if (v == NULL)
		return 0;

	v->changed = 1;
	v->vFloat = val;
}

int
odk_value_set_double (odkValue * v, double val)
{
	if (v == NULL)
		return 0;

	v->changed = 1;
	v->vDouble = val;
}

int
odk_value_set_blob (odkValue * v, int size, void *val)
{
	if (v == NULL)
		return 0;

	v->changed = 1;
	v->vBlob = val;
	v->size = size;
}

int
odk_value_set_date (odkValue * v, odkDate * val)
{
	if (v == NULL)
		return 0;

	v->changed = 1;
	v->vDate = val;
}

int
odk_value_set_time (odkValue * v, odkTime * val)
{
	if (v == NULL)
		return 0;

	v->changed = 1;
	v->vTime = val;
}

int
odk_value_set_datetime (odkValue * v, odkDateTime * val)
{
	if (v == NULL)
		return 0;

	v->changed = 1;
	v->vDateTime = val;
}

char *
odk_value_get (odkValue * v)
{
	char *ret = NULL;

	switch (v->type) {
	case ODKDA_BOOL:
		if (v->vBool) {
			ret = strcopy ("True");
		} else {
			ret = strcopy ("False");
		}
		break;
	case ODKDA_CHAR:
		ret = (char *) malloc (sizeof (char) * 4);
		sprintf (ret, "%i", v->vChar);
		break;
	case ODKDA_SHORT:
		ret = (char *) malloc (sizeof (char) * 6);
		sprintf (ret, "%i", v->vShort);
		break;
	case ODKDA_INT:
		ret = (char *) malloc (sizeof (char) * 10);
		sprintf (ret, "%i", v->vInt);
		break;
	case ODKDA_LONG:
		ret = (char *) malloc (sizeof (char) * 16);
		sprintf (ret, "%i", v->vLong);
		break;
	case ODKDA_FLOAT:
		ret = (char *) malloc (sizeof (char) * 20);
		sprintf (ret, "%f", v->vFloat);
		break;
	case ODKDA_DOUBLE:
		ret = (char *) malloc (sizeof (char) * 25);
		sprintf (ret, "%f", (float)v->vDouble);
		break;
	case ODKDA_TEXT:
		ret = strcopy (v->vString);
		break;
	case ODKDA_BLOB:
		ret = (char *) v->vBlob;
		break;
	default:
		ret = NULL;
		break;
	}

	return ret;
}

bool
odk_value_get_bool (odkValue * v)
{
	if (v == NULL)
		return 0;

	return v->vBool;
}

char
odk_value_get_char (odkValue * v)
{
	if (v == NULL)
		return 0;

	switch (v->type) {
	case ODKDA_BOOL:
		return (char) v->vBool;
	case ODKDA_CHAR:
		return v->vChar;
	default:
		break;
	}

	return 0;
}

short
odk_value_get_short (odkValue * v)
{
	if (v == NULL)
		return 0;

	switch (v->type) {
	case ODKDA_BOOL:
		return (short) v->vBool;
	case ODKDA_CHAR:
		return (short) v->vChar;
	case ODKDA_SHORT:
		return v->vShort;
	default:
		break;
	}

	return 0;
}

int
odk_value_get_int (odkValue * v)
{
	if (v == NULL)
		return 0;

	switch (v->type) {
	case ODKDA_BOOL:
		return (int) v->vBool;
	case ODKDA_CHAR:
		return (int) v->vChar;
	case ODKDA_SHORT:
		return (int) v->vShort;
	case ODKDA_INT:
		return v->vInt;
	default:
		break;
	}

	return 0;
}

long
odk_value_get_long (odkValue * v)
{
	if (v == NULL)
		return 0;

	switch (v->type) {
	case ODKDA_BOOL:
		return (long) v->vBool;
	case ODKDA_CHAR:
		return (long) v->vChar;
	case ODKDA_SHORT:
		return (long) v->vShort;
	case ODKDA_INT:
		return (long) v->vInt;
	case ODKDA_LONG:
		return v->vLong;
	default:
		break;
	}

	return 0;
}

float
odk_value_get_float (odkValue * v)
{
	if (v == NULL)
		return 0;

	switch (v->type) {
	case ODKDA_BOOL:
		return (float) v->vBool;
	case ODKDA_CHAR:
		return (float) v->vChar;
	case ODKDA_SHORT:
		return (float) v->vShort;
	case ODKDA_INT:
		return (float) v->vInt;
	case ODKDA_LONG:
		return (float) v->vLong;
	case ODKDA_FLOAT:
		return v->vFloat;
	case ODKDA_DOUBLE:
		return (float) v->vDouble;
	default:
		break;
	}

	return 0.0f;
}

double
odk_value_get_double (odkValue * v)
{
	if (v == NULL)
		return 0;

	switch (v->type) {
	case ODKDA_BOOL:
		return (double) v->vBool;
	case ODKDA_CHAR:
		return (double) v->vChar;
	case ODKDA_SHORT:
		return (double) v->vShort;
	case ODKDA_INT:
		return (double) v->vInt;
	case ODKDA_LONG:
		return (double) v->vLong;
	case ODKDA_FLOAT:
		return (double) v->vFloat;
	case ODKDA_DOUBLE:
		return v->vDouble;
	default:
		break;
	}
	return 0;
}

void *
odk_value_get_blob (odkValue * v)
{
	if (v == NULL)
		return NULL;
	return v->vBlob;
}

odkDate *
odk_value_get_date (odkValue * v)
{
	if (v == NULL)
		return NULL;

	return v->vDate;
}

odkTime *
odk_value_get_time (odkValue * v)
{
	if (v == NULL)
		return NULL;

	return v->vTime;
}

odkDateTime *
odk_value_get_datetime (odkValue * v)
{
	if (v == NULL)
		return NULL;

	return v->vDateTime;
}


int odk_value_get_type(odkValue *v){
	if (v == NULL)
		return 0;
		
	return v->type;
}

int odk_value_get_size(odkValue *v){
	if (v == NULL)
		return 0;
		
	return v->size;
}

bool odk_value_is_changed(odkValue *v){
	if (v == NULL)
		return 0;
		
	return v->changed;
}
