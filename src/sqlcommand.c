/*
ODKDA: Data Access Library
        SQL Command Object
		
Copyright (C) 2006 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdio.h>
#include <malloc.h>
#include <string.h>

#include <odkutils.h>
#include <odkutils_xml.h>

#include <odkda/common.h>
#include <odkda/column.h>
#include <odkda/date.h>
#include <odkda/time.h>
#include <odkda/datetime.h>
#include <odkda/value.h>
#include <odkda/command.h>
#include <odkda/sqlcommand.h>

void
odk_sqlcommand_delete (odkSQLCommand * c)
{
	int i;
	
	free (SQLCOMMAND (c).sql);

	for (i = 0; i < SQLCOMMAND (c).count; i++){
		object_unref(SQLCOMMAND (c).binds[i]->value);
		free(SQLCOMMAND (c).binds[i]);
	}

	free(SQLCOMMAND (c).binds);
	
	if(SQLCOMMAND (c).names)
		odk_hashtable_free(SQLCOMMAND (c).names, NULL);
}

odkSQLCommand *
odk_sqlcommand_new (char *sql)
{
	return odk_sql_new(sql);
}

odkSQLCommand *
odk_sql_new(char *sql)
{
	int i;
	odkSQLCommand *c = (odkSQLCommand *) malloc (sizeof (odkSQLCommand));
	int ovec[30];
	odkSList *binds;
	BindValue *b;
	char buff[100];
	odkRegex *re;
	odkSList *rr;
	odkRegMatch *m;
	
	object_register (c, "odkSQLCommand");
	odk_command_init (c);

	object_append_destructor (c, (FREE_FUNC)&odk_sqlcommand_delete);

	COMMAND (c).type = 1;
	SQLCOMMAND (c).sql = strcopy (sql);
	SQLCOMMAND (c).count = 0;
	SQLCOMMAND (c).names = NULL;
	
	binds = odk_slist_new(0);
	
	re = odk_regex_new("\\?|:\\w[\\w\\d]*\\b", 0, 0);
	rr = odk_regex_match_all(re, sql);
	
	for SLIST_ITERATOR(m, rr, odkRegMatch*){
		b = malloc(sizeof(BindValue));
		b->start = m->start;
		b->end = m->end;
		b->literal = 0;
		b->value = NULL;
		
		odk_slist_append(binds, b);
		
		if(sql[m->start] != '?'){
			memset(buff, 0, 100);
			memcpy(buff, (char*)(sql + m->start+1), m->end - m->start - 1);
			
			if (SQLCOMMAND (c).names == NULL)
				SQLCOMMAND (c).names = odk_hashtable_new(0);
			odk_hashtable_insert(SQLCOMMAND (c).names, buff, (void*)odk_slist_count(binds));
		}
	}
	
	SQLCOMMAND(c).count = odk_slist_count(binds);
	SQLCOMMAND(c).binds = (BindValue**)odk_slist_toarray(binds);

	if (SQLCOMMAND(c).count == 0)
		SQLCOMMAND(c).binds = NULL;
	
	odk_slist_free(binds, NULL);
	return c;
}

int odk_sql_bind(odkSQLCommand *cmd, int n, odkValue *val){
	if (cmd == NULL)
		return 0;
	
	if (n > SQLCOMMAND (cmd).count)
		return 0;
	
	if (SQLCOMMAND (cmd).binds[n]->value != NULL)
		object_unref(SQLCOMMAND(cmd).binds[n]->value);
	
	object_ref(val);
	SQLCOMMAND(cmd).binds[n]->value = val;

	return 1;
}

int odk_sql_bind_int(odkSQLCommand *cmd, int n, int num){
	if (cmd == NULL)
		return 0;
	
	if (n > SQLCOMMAND (cmd).count)
		return 0;
	
	if (SQLCOMMAND(cmd).binds[n]->value != NULL){
		if (odk_value_get_type(SQLCOMMAND(cmd).binds[n]->value) != ODKDA_INT){
			object_unref(SQLCOMMAND(cmd).binds[n]->value);
			SQLCOMMAND(cmd).binds[n]->value = odk_value_new(ODKDA_INT);
		}
	}else{
		SQLCOMMAND(cmd).binds[n]->value = odk_value_new(ODKDA_INT);
	}
		
	odk_value_set_int(SQLCOMMAND(cmd).binds[n]->value, num);

	return 1;
}

int odk_sql_bind_float(odkSQLCommand *cmd, int n, float num){
	if (cmd == NULL)
		return 0;
	
	if (n > SQLCOMMAND (cmd).count)
		return 0;
	
	if (SQLCOMMAND(cmd).binds[n]->value != NULL){
		if (odk_value_get_type(SQLCOMMAND(cmd).binds[n]->value) != ODKDA_FLOAT){
			object_unref(SQLCOMMAND(cmd).binds[n]->value);
			SQLCOMMAND(cmd).binds[n]->value = odk_value_new(ODKDA_FLOAT);
		}
	}else{
		SQLCOMMAND(cmd).binds[n]->value = odk_value_new(ODKDA_FLOAT);
	}
		
	odk_value_set_float(SQLCOMMAND(cmd).binds[n]->value, num);

	return 1;
}

int odk_sql_bind_string(odkSQLCommand *cmd, int n, char *str){
	if (cmd == NULL)
		return 0;
	
	if (n > SQLCOMMAND (cmd).count)
		return 0;
	
	if (SQLCOMMAND(cmd).binds[n]->value != NULL){
		if (odk_value_get_type(SQLCOMMAND(cmd).binds[n]->value) != ODKDA_TEXT){
			object_unref(SQLCOMMAND(cmd).binds[n]->value);
			SQLCOMMAND(cmd).binds[n]->value = odk_value_new(ODKDA_TEXT);
		}
	}else{
		SQLCOMMAND(cmd).binds[n]->value = odk_value_new(ODKDA_TEXT);
	}
		
	odk_value_set(SQLCOMMAND(cmd).binds[n]->value, str);

	return 1;
}

int odk_sql_bind_literal(odkSQLCommand *cmd, int n, char *str){
	if (cmd == NULL)
		return 0;
	
	if (n > SQLCOMMAND (cmd).count)
		return 0;
	
	if (SQLCOMMAND(cmd).binds[n]->value != NULL){
		if (odk_value_get_type(SQLCOMMAND(cmd).binds[n]->value) != ODKDA_TEXT){
			object_unref(SQLCOMMAND(cmd).binds[n]->value);
			SQLCOMMAND(cmd).binds[n]->value = odk_value_new(ODKDA_TEXT);
		}
	}else{
		SQLCOMMAND(cmd).binds[n]->value = odk_value_new(ODKDA_TEXT);
	}
		
	odk_value_set(SQLCOMMAND(cmd).binds[n]->value, str);
	SQLCOMMAND(cmd).binds[n]->literal = 1;
	
	return 1;
}

int odk_sql_bind_blob(odkSQLCommand *cmd, int n, int size, void *blob){
	if (cmd == NULL)
		return 0;
	
	if (n > SQLCOMMAND (cmd).count)
		return 0;
	
	if (SQLCOMMAND(cmd).binds[n]->value != NULL)
		object_unref(SQLCOMMAND(cmd).binds[n]->value);
		
	SQLCOMMAND(cmd).binds[n]->value = odk_value_new(ODKDA_BLOB);
	odk_value_set_blob(SQLCOMMAND(cmd).binds[n]->value, size, blob);

	return 1;
}

int odk_sql_bind_named(odkSQLCommand *cmd, char *pcn, odkValue *val){
	int n = 0;
	
	if (cmd == NULL)
		return 0;
	
	if (n > SQLCOMMAND (cmd).count)
		return 0;
	
	if (SQLCOMMAND (cmd).names == NULL)
		return 0;
		
	n = (int)odk_hashtable_lookup(SQLCOMMAND(cmd).names, pcn);
	
	if (n < 1)
		return 0;
		
	return odk_sql_bind(cmd, n-1, val);
}

int odk_sql_bind_named_int(odkSQLCommand *cmd, char *pcn, int num){
	int n = 0;
	
	if (cmd == NULL)
		return 0;
	
	if (n > SQLCOMMAND (cmd).count)
		return 0;
	
	if (SQLCOMMAND (cmd).names == NULL)
		return 0;
	
	n = (int)odk_hashtable_lookup(SQLCOMMAND(cmd).names, pcn);
	
	if (n < 1)
		return 0;
		
	return odk_sql_bind_int(cmd, n-1, num);
}

int odk_sql_bind_named_float(odkSQLCommand *cmd, char *pcn, float num){
	int n = 0;
	
	if (cmd == NULL)
		return 0;
	
	if (n > SQLCOMMAND (cmd).count)
		return 0;
	
	if (SQLCOMMAND (cmd).names == NULL)
		return 0;
		
	n = (int)odk_hashtable_lookup(SQLCOMMAND(cmd).names, pcn);
	
	if (n < 1)
		return 0;
		
	return odk_sql_bind_float(cmd, n-1, num);
}

int odk_sql_bind_named_string(odkSQLCommand *cmd, char *pcn, char *str){
	int n = 0;
	
	if (cmd == NULL)
		return 0;
	
	if (n > SQLCOMMAND (cmd).count)
		return 0;
	
	if (SQLCOMMAND (cmd).names == NULL)
		return 0;
		
	n = (int)odk_hashtable_lookup(SQLCOMMAND(cmd).names, pcn);
	
	if (n < 1)
		return 0;
		
	return odk_sql_bind_string(cmd, n-1, str);
}

int odk_sql_bind_named_literal(odkSQLCommand *cmd, char *pcn, char *str){
	int n = 0;
	
	if (cmd == NULL)
		return 0;
	
	if (n > SQLCOMMAND (cmd).count)
		return 0;
	
	if (SQLCOMMAND (cmd).names == NULL)
		return 0;
		
	n = (int)odk_hashtable_lookup(SQLCOMMAND(cmd).names, pcn);
	
	if (n < 1)
		return 0;
		
	return odk_sql_bind_literal(cmd, n-1, str);
}

int odk_sql_bind_named_blob(odkSQLCommand *cmd, char *pcn, int size, void *blob){
	int n = 0;
	
	if (cmd == NULL)
		return 0;
	
	if (n > SQLCOMMAND (cmd).count)
		return 0;
	
	if (SQLCOMMAND (cmd).names == NULL)
		return 0;
		
	n = (int)odk_hashtable_lookup(SQLCOMMAND(cmd).names, pcn);
	
	if (n < 1)
		return 0;
		
	return odk_sql_bind_blob(cmd, n-1, size, blob);
}

char *odk_sql_prepare(odkSQLCommand *cmd){
	int i, j, k, v, tlen ,cpos;
	char *prepared, *sql, *tmp;
	
	if (cmd == NULL)
		return NULL;
	
	if (SQLCOMMAND(cmd).count == 0)
		return strcopy(SQLCOMMAND(cmd).sql);	
	
	tlen = strlen(SQLCOMMAND (cmd).sql);
	
	for (i = 0; i < SQLCOMMAND(cmd).count; i++){
		if(SQLCOMMAND(cmd).binds[i]->value == NULL)
			return NULL;
		
		switch(SQLCOMMAND(cmd).binds[i]->value->type){
			case ODKDA_INT:
				tlen += 15;
				break;
			case ODKDA_TEXT:
				tlen += SQLCOMMAND(cmd).binds[i]->value->size + 10;
				break;
			case ODKDA_FLOAT:
				tlen += 40;
				break;
			case ODKDA_BLOB:
				tlen += SQLCOMMAND(cmd).binds[i]->value->size;
				break;
			default:
				break;
		}
	}
	
	prepared = (char*)malloc(tlen * sizeof(char));
	memset(prepared, 0, tlen);
	sql = SQLCOMMAND(cmd).sql;
	
	i = 0;
	j = 0; //sql Offset
	v = 0; //
	k = 0; //Prepared String Offset
	cpos = 0;
	
	for (i = 0; i < SQLCOMMAND(cmd).count; i++){
		v = SQLCOMMAND(cmd).binds[i]->start - j;
		memcpy((char*)(prepared+k), (char*)(sql+j), v);
		k += v;
		
		if (SQLCOMMAND(cmd).binds[i]->value->type == ODKDA_TEXT){
			if(!SQLCOMMAND(cmd).binds[i]->literal){
				prepared[k] = '\'';
				k++;
			}
				
			memcpy((char*)(prepared+k), SQLCOMMAND(cmd).binds[i]->value->vString, SQLCOMMAND(cmd).binds[i]->value->size);
			k += SQLCOMMAND(cmd).binds[i]->value->size;
				
			if(!SQLCOMMAND(cmd).binds[i]->literal){
				prepared[k] = '\'';
				k++;
			}
		}else{
			tmp = odk_value_get(SQLCOMMAND(cmd).binds[i]->value);
			memcpy((char*)(prepared+k), tmp, strlen(tmp));
			k += strlen(tmp);
			free(tmp);
		}
		
		j += v + (SQLCOMMAND(cmd).binds[i]->end - SQLCOMMAND(cmd).binds[i]->start);
	}
	
	memcpy((char*)(prepared+k), (char*)(sql+j), strlen(sql)-j);
	
	return prepared;
}
