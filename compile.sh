#!/bin/bash

if [ -n $1 ]; then
if [ "$1" = "install" ]; then
	PREFIX=`cat bin/prefix`
	ETCPATH=`cat bin/etc`
	if [ -n "$2" ]; then
		PREFIX="$2"
        ETCPATH="$PREFIX/etc"
	fi	

	INCPATH="$PREFIX/include/odk"
	LIBPATH="$PREFIX/lib"
	
	mkdir -p $PREFIX
	mkdir -p $INCPATH
	mkdir -p $LIBPATH
	mkdir -p $ETCPATH
    mkdir -p $ETCPATH/odkda2
    
	mkdir -p $PREFIX/bin
	echo "Installing: Libraries"
	cp bin/*.so $LIBPATH
	#cp bin/*.a $LIBPATH
	cp bin/odkda-config $PREFIX/bin
	ldconfig
	echo "Installing: Development Headers"
	mkdir -p $INCPATH/odkda
	cp include/odkda/*.h $INCPATH/odkda/
	cp include/*.h $INCPATH/
    
    cp bin/odkda.conf $ETCPATH/odkda2
    cp bin/*.xml $ETCPATH/odkda2
    
	exit
fi
fi

if [ -n "$1" ]; then
	PREFIX="$1"
	ETCPATH="$PREFIX/etc"
else
	PREFIX="/usr"
	ETCPATH="/etc"
fi

VERSION="0.5.8.1"
	
INCPATH="$PREFIX/include/odk"
LIBPATH="$PREFIX/lib"
CONFFILE="$ETCPATH/odkda.conf"

rm -f config.h

rm -fr bin	
mkdir -p bin
export CFLAGS="-fPIC -g -Wall"

echo "Autoconfiguring (best guess)"

rm -f mysqltest.c
rm -f mysqltest

echo "
#include <malloc.h>
#include <stdio.h>
#include <mysql.h>

int main(){	
	
	return 1;
}

" > mysqltest.c

#Test for MySQL
if (mysql_config --libs 2>&1 >/dev/null); then
    if (gcc mysqltest.c `mysql_config --libs --cflags` -o mysqltest 2>/dev/null >/dev/null); then
	    echo "  [Detected MySQL]"
	    WITHMYSQL="Yes"
    	DEFS="$DEFS -D_USE_MYSQL"
    else
	    WITHMYSQL="No"
    fi
else
	WITHMYSQL="No"
fi

rm -f mysqltest.c
rm -f mysqltest

#FreeTDS Test Program
rm -f tdstest.c
rm -f tdstest

echo "
#include <malloc.h>
#include <stdio.h>
#include <sybdb.h>

int main(){	
	
	return 1;
}

" > tdstest.c

if (gcc tdstest.c -o tdstest 2>/dev/null >/dev/null); then
	echo "  [Detected FreeTDS]"
	WITHTDS="Yes"
	DEFS="$DEFS -D_USE_TDS"
else
	WITHTDS="No"
fi

rm -f tdstest.c
rm -f tdstest

#Test from SQLite2
if (pkg-config --silence-errors --libs sqlite 2>/dev/null >/dev/null); then
	echo "  [Detected SQLite]"
	WITHSQLITE="Yes"
	DEFS="$DEFS -D_USE_SQLITE"
else
	WITHSQLITE="No"
fi

#Test for SQLite3
if (pkg-config --silence-errors --libs sqlite3 2>/dev/null >/dev/null); then
	echo "  [Detected SQLite3]"
	WITHSQLITE3="Yes"
	DEFS="$DEFS -D_USE_SQLITE3"
else
	WITHSQLITE3="No"
fi

#Test from PostgreSQL
if (pg_config --libdir 2>/dev/null >/dev/null); then
	echo "  [Detected PostgreSQL]"
	WITHPOSTGRES="Yes"
	DEFS="$DEFS -D_USE_POSTGRES"
else
	WITHPOSTGRES="No"
fi

if ( ls $ORACLE_HOME/rdbms/public/oci.h 2>/dev/null >/dev/null); then
	echo "  [Detected Oracle]"
	WITHORACLE="Yes"
	DEFS="$DEFS -D_USE_ORACLE"
else
	WITHORACLE="No"
fi

echo $PREFIX > bin/prefix
echo $ETCPATH > bin/etc
echo "char *configs[2] = {\"$ETCPATH/odkda.conf\", \"odkda.conf\"};" >> config.h
echo "static char* PREFIX = \"$PREFIX\";" >> config.h
echo "static char* VERSION = \"$VERSION\";" >> config.h
echo "static char* INCPATH = \"$INCPATH\";" >> config.h
echo "static char* LIBPATH = \"$LIBPATH\";" >> config.h
echo "static char* ETCPATH = \"$ETCPATH\";" >> config.h
echo "static char* CONFFILE = \"$ETCPATH/odkda2/odkda.conf\";" >> config.h
	
mkdir -p bin
export CFLAGS="-fPIC -g"
export DIRS='-I./include '
export LIBS=''

echo " "
echo "Compiling ODKDA2"

echo " Compiling Sources"
gcc src/provider.c -I./include -I./ -D_HAVE_CONFIG_ $CFLAGS `odkutils-config --cflags` -c -o bin/provider.o
gcc src/connection.c -I./include $CFLAGS `odkutils-config --cflags` -c -o bin/connection.o
gcc src/column.c -I./include $CFLAGS `odkutils-config --cflags` -c -o bin/column.o
gcc src/value.c -I./include $CFLAGS `odkutils-config --cflags` -c -o bin/value.o
gcc src/command.c -I./include $CFLAGS `odkutils-config --cflags` -c -o bin/command.o
gcc src/sqlcommand.c -I./include $CFLAGS `odkutils-config --cflags` -c -o bin/sqlcommand.o
gcc src/schema.c -I./include $CFLAGS `odkutils-config --cflags` -c -o bin/schema.o
gcc src/exts.c -I./include $CFLAGS `odkutils-config --cflags` -c -o bin/exts.o

echo " Creating Library"
gcc bin/provider.o bin/connection.o bin/column.o bin/value.o bin/command.o bin/sqlcommand.o bin/schema.o bin/exts.o `odkutils-config --libs --cflags` -shared -Wl,-soname,libodkda2.so $CFLAGS -o bin/libodkda2.so

echo " Compiling Plugins"

if [ $WITHMYSQL == "Yes" ]; then
	echo "   * MySQL"
	gcc src/plugins/mysql.c -I./include -L./bin -lodkda2 $CFLAGS -shared -Wl,-soname,libodkda2-mysql.so `odkutils-config --libs --cflags` `mysql_config --libs --cflags` -o bin/libodkda2-mysql.so
	cp src/plugins/mysql.xml bin/
fi

if [ $WITHTDS == "Yes" ]; then
	echo "   * FreeTDS (MS SQL, Sybase)"
	gcc src/plugins/tds.c -I./include -L./bin -lodkda2 $CFLAGS -shared -Wl,-soname,libodkda2-tds.so `odkutils-config --libs --cflags` -o bin/libodkda2-tds.so
	cp src/plugins/tds.xml bin/
fi

if [ $WITHPOSTGRES == "Yes" ]; then
	echo "   * PostgreSQL"
	gcc src/plugins/pgsql.c -I./include -L./bin -lodkda2 -lpq $CFLAGS -shared -Wl,-soname,libodkda2-pgsql.so `odkutils-config --libs --cflags` -I`pg_config --includedir` -L`pg_config --libdir` -o bin/libodkda2-pgsql.so
	cp src/plugins/pgsql.xml bin/
fi

if [ $WITHSQLITE3 == "Yes" ]; then
	echo "   * SQLite 3"
	gcc src/plugins/sqlite3.c -I./include -L./bin -lodkda2 $CFLAGS -shared -Wl,-soname,libodkda2-sqlite3.so `odkutils-config --libs --cflags` `pkg-config --libs --cflags sqlite3` -o bin/libodkda2-sqlite3.so
	cp src/plugins/sqlite3.xml bin/
fi

echo " Creating odkda-config Script"
gcc src/config.c -fPIC -I./ -D_HAVE_CONFIG_ `odkutils-config --static --libs --cflags` -o bin/odkda-config

pushd bin
	for f in *.xml; do
		./odkda-config --rlocal $f
    done
popd
