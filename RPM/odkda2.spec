#
# spec file for package odkDA2
#
# Copyright  (c)  2006  Carlos Daniel Ruvalcaba Valenzuela
#

Name: odkda2
Summary: The package provides a complete set of utilities for database programming.
Version: 0.5.8.1
Release: 1
License: LGPL
Group: Libraries
URL: http://odkit.sourceforge.net/
Source: %{name}-%{version}.tar.gz
Provides: odkda2
Requires: odkutils
#Prereq: bash gcc
BuildPrereq: odkutils-devel
Buildroot: %{_tmppath}/%{name}-%{version}-%{release}-root

%description 
The package odkDA provides a common API to access many databases.

%package devel
Summary: odkDA2 development files
Group: Development/Library
Provides: odkutils-devel

%description devel
This package contains necessary header files for odkDA2 development.

%package mysql
Summary: odkDA2 MySQL Plugin
Group: Library
Requires: odkda2
Provides: odkda2-mysql

%description mysql
This package contains necessary plugin for odkDA2 development with MySQL.

%post mysql
odkda-config --register /etc/odkda2/mysql.xml

%package pgsql
Summary: odkDA2 PostgreSQL Plugin
Group: Library
Requires: odkda2
Provides: odkda2-pgsql

%description pgsql
This package contains necessary plugin for odkDA2 development with PostgreSQL.

%post pgsql
odkda-config --register /etc/odkda2/pgsql.xml

%package sqlite3
Summary: odkDA2 SQLite3 Plugin
Group: Library
Requires: odkda2
Provides: odkda2-sqlite3

%description sqlite3
This package contains necessary plugin for odkDA2 development with SQLite3.

%post sqlite3
odkda-config --register /etc/odkda2/sqlite3.xml

%prep 
%setup -q

%build 
sh compile.sh

%install 
rm -fr %{buildroot}
sh compile.sh install %{buildroot}/usr
cp -rf %{buildroot}/usr/etc %{buildroot}
rm -fr %{buildroot}/usr/etc
rm -f %{buildroot}/etc/odkda2/odkda.conf

%clean 
rm -fr %{buildroot}

%post 
/sbin/ldconfig

%files 
%defattr(-,root,root) 
%{_libdir}/libodkda2.so
%{_bindir}/*

%files devel
%defattr(-,root,root) 
%{_includedir}/* 
#%{_libdir}/*.a

%files mysql
%defattr(-,root,root) 
%{_libdir}/libodkda2-mysql.so
%{_sysconfdir}/odkda2/mysql.xml

%files pgsql
%defattr(-,root,root) 
%{_libdir}/libodkda2-pgsql.so
%{_sysconfdir}/odkda2/pgsql.xml

%files sqlite3
%defattr(-,root,root) 
%{_libdir}/libodkda2-sqlite3.so
%{_sysconfdir}/odkda2/sqlite3.xml

%changelog 

* Wed Jun 20 2007 Carlos Ruvalcaba <clsdaniel@users.sourceforge.net>
- Updated to version 0.5.8.1
- Updated plugin registration process

* Mon Apr 02 2007 Carlos Ruvalcaba <clsdaniel@users.sourceforge.net>
- Updated to version 0.5.6

* Thu Dec 14 2006 Carlos Ruvalcaba <clsdaniel@users.sourceforge.net>
- Updated minally descriptions
- Updated to latest version

* Mon Dec 11 2006 Carlos Ruvalcaba <clsdaniel@users.sourceforge.net>
- initial RPM spec
