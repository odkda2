#include <stdio.h>
#include <sys/time.h>

#include <odkutils.h>
#include <odkutils_xml.h>
#include <odkutils_interop.h>

#include <odkda2.h>

odkConfigStore *dpconf;

int benchmark(char *provider){
	odkDataProvider *dp;
	odkConnection *conn;
	odkCommand *cmd;
	odkValue **val;
	char buffer[100];
	char *host, *user, *pwd, *db;
	int i, j, port, ret;
	double t1, t2, elapsed, prom, prom_fetch;
    struct timeval tp;
  
	dp = odk_dataprovider_new(provider);
	
	
	if (dp == NULL){
		printf("Unable to create provider\n");
		return 0;
	}
	
	conn = odk_connection_new(dp);
	
	if (conn == NULL){
		printf("Unable to create connection object\n");
		return 0;
	}
	
	sprintf(buffer, "/%s/host", provider);
	host = odk_config_key_get(dpconf, buffer);

	sprintf(buffer, "/%s/port", provider);
	port = odk_config_key_geti(dpconf, buffer);

	sprintf(buffer, "/%s/user", provider);
	user = odk_config_key_get(dpconf, buffer);
	
	sprintf(buffer, "/%s/passwd", provider);
	pwd = odk_config_key_get(dpconf, buffer);
	
	sprintf(buffer, "/%s/db", provider);
	db = odk_config_key_get(dpconf, buffer);
	
	ret = odk_connection_open(conn, host, port, user, pwd, db);
	
	if (ret == 0){
		printf("Unable to create connect\n");
	}

	cmd = (odkCommand*)odk_sql_new("drop table odkbench");
	odk_connection_execute(conn, cmd);
	object_unref(cmd);
	
	cmd = (odkCommand*)odk_sql_new("create table odkbench(numa int, numb int)");
	odk_connection_execute(conn, cmd);
	object_unref(cmd);
	
	prom = 0.0;
	printf("Insert Test\n");
	for (i = 0; i < 10000; i++){
		sprintf(buffer, "insert into odkbench values (%i, %i)", i, i);
		cmd = (odkCommand*)odk_sql_new(buffer);
	
		ret = gettimeofday(&tp, NULL);
	    t1 = (double)tp.tv_sec + (1.e-6)*tp.tv_usec;
		odk_connection_execute(conn, cmd);

	    ret = gettimeofday(&tp, NULL);
    	t2 = (double)tp.tv_sec + (1.e-6)*tp.tv_usec;
	    elapsed = t2 - t1;
		prom += elapsed;
		
		object_unref(cmd);
	}
	
	printf("  Inserts: 10000\n");
	printf("  Elapsed: %f segs\n", prom);
	printf("  Per Call: %f segs\n", (prom/10000));
	
	prom = 0.0;
	printf("Update Test\n");
	for (i = 0; i < 10000; i++){
		sprintf(buffer, "update odkbench set numa = %i where numb = %i", 10000-i, i);
		cmd = (odkCommand*)odk_sql_new(buffer);
	
		ret = gettimeofday(&tp, NULL);
	    t1 = (double)tp.tv_sec + (1.e-6)*tp.tv_usec;
		odk_connection_execute(conn, cmd);

	    ret = gettimeofday(&tp, NULL);
    	t2 = (double)tp.tv_sec + (1.e-6)*tp.tv_usec;
	    elapsed = t2 - t1;
		prom += elapsed;
		
		object_unref(cmd);
	}
	
	printf("  Updates: 10000\n");
	printf("  Elapsed: %f segs\n", prom);
	printf("  Per Call: %f segs\n", (prom/10000));
	
	prom = 0.0;
	prom_fetch = 0.0;
	printf("Small Query Fetch Test\n");
	for (i = 0; i < 10; i++){
		sprintf(buffer, "select * from odkbench");
		cmd = (odkCommand*)odk_sql_new(buffer);
	
		ret = gettimeofday(&tp, NULL);
	    t1 = (double)tp.tv_sec + (1.e-6)*tp.tv_usec;
		odk_connection_execute(conn, cmd);
		
		while(val = odk_command_fetch(cmd)){
			object_unref(val[0]);
			object_unref(val[1]);
		}
	    ret = gettimeofday(&tp, NULL);
    	t2 = (double)tp.tv_sec + (1.e-6)*tp.tv_usec;
	    elapsed = t2 - t1;
		prom += elapsed;
		
		object_unref(cmd);
	}
	
	printf("  Queries: 10\n");
	printf("  Elapsed: %f segs\n", prom);
	printf("  Per Call: %f segs\n", (prom/10));
	
	odk_connection_close(conn);
	object_unref(conn);
	object_unref(dp);
	
	return 0;
}

int main(int argc, char **argv){
	odk_log_init("logs.txt",3,0);
	dpconf = (odkConfigStore*)odk_xmlconfig_new("providers.conf", "config");
	odk_config_load(dpconf);
	benchmark(argv[1]);
	return 0;
}
