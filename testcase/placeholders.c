
#include <stdio.h>
#include <malloc.h>
#include <string.h>

#include <odkutils.h>
#include <odkutils_xml.h>
#include <odkda2.h>

int main(int argc, char **argv){
	odkSQLCommand *cmd;
	char *ret;
	
	cmd = odk_sql_new("insert into test values(:id, ?, :name4, :phone)");
	odk_sql_bind_int(cmd, 1, 99);
	odk_sql_bind_named_int(cmd, "id", 7);
	odk_sql_bind_named_string(cmd, "name", "zoot");
	odk_sql_bind_string(cmd, 3, "8488448");
	
	ret = odk_sql_prepare(cmd);
	
	if (ret == NULL){
		printf("Error while praparing SQL String\n");
	}else{
		printf("%s\n", ret);
	}
	
	return 0;
}
