
#include <stdio.h>
#include <string.h>

#include <odkutils.h>
#include <odkutils_xml.h>
#include <odkda2.h>

typedef struct {
	char *name;
	char *address;
	char *phone;
	char *email;
} Contact;

typedef struct {
	char name[100];
	char address[100];
	char phone[100];
	char email[100];
} Contact2;

odkFieldDef ContactDef[4] = {{100, ODKDA_TEXT, ODK_STRING_PTR},
							{100, ODKDA_TEXT, ODK_STRING_PTR},
							{100, ODKDA_TEXT, ODK_STRING_PTR},
							{100, ODKDA_TEXT, ODK_STRING_PTR}};
							
odkFieldDef ContactDef2[4] = {{100, ODKDA_TEXT, 0},
							{100, ODKDA_TEXT, 0},
							{100, ODKDA_TEXT, 0},
							{100, ODKDA_TEXT, 0}};

int main(int argc, char **argv){
	odkDataProvider *dp = NULL;
	odkConnection *conn = NULL;
	odkCommand *cmd = NULL;
	Contact *cContact;
	int ret;
	
	dp = odk_dataprovider_new("sqlite3");
	if (dp == NULL){
		printf("Unable to create DataProvider\n");
		return 0;
	}
	
	conn = odk_connection_new(dp);
	if (conn == NULL){
		printf("Unable to create Connection\n");
		return 0;
	}
	odk_connection_open(conn, "test.db", 0, NULL, NULL, "test.db");
	
	cmd = (odkCommand*)odk_sql_new("SELECT * FROM contacts");
	if (cmd == NULL){
		printf("Unable to create Command\n");
		return 0;
	}
	
	ret = odk_connection_execute(conn, cmd);
	printf("Execute Returns %i\n", ret);
	
	while(cContact = odk_command_fetchex(cmd, 4, ContactDef)){
		printf("Contact:\n");
		printf("\tName: %s\n", cContact->name);
		printf("\tAddress: %s\n", cContact->address);
		printf("\tPhone: %s\n", cContact->phone);
		printf("\tE-Mail: %s\n", cContact->email);
		printf("\n");
		free(cContact);
	}
	
	odk_connection_close(conn);
	
	object_unref(cmd);
	object_unref(conn);
	object_unref(dp);
	
	return 0;
}
