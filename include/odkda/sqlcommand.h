/*
ODKDA: Data Access Library
        SQL Command Object
		
Copyright (C) 2006 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _SQLCOMMAND_H
#define _SQLCOMMAND_H

typedef struct {
	int start;
	int end;
	odkValue *value;
	char literal;
} BindValue;

typedef struct {
    char *sql;			//SQL String
	int count;			//Amount of binded values
	BindValue **binds;
	odkHashTable *names;
} _odkSQLCommand;

#define CLASS_SQLCOMMAND CLASS_COMMAND _odkSQLCommand Class_SQLCommand;

typedef struct {
CLASS_SQLCOMMAND} odkSQLCommand;

#define SQLCOMMAND(o) (((odkSQLCommand*)o)->Class_SQLCommand)


/* @FUNC: odk_sql_new
	@DESC: Creates a new SQLCommand
	@PARAM[sql]: SQL Query string.
	@RETS: A valid SQLCommand object on success, NULL otherwise.
*/
odkSQLCommand *odk_sql_new(char *sql);

/* @FUNC: odk_sql_bind
	@DESC: Binds a value to an unnamed placeholder from the SQLCommand.
	@PARAM[cmd]: A valid SQLCommand object.
	@PARAM[n]: Position of the placeholder, starts on 0.
	@PARAM[val]: Value to bind (odkValue).
	@RETS: True on success, false otherwise.
*/
int odk_sql_bind(odkSQLCommand *cmd, int n, odkValue *val);

/* @FUNC: odk_sql_bind_int
	@DESC: Binds a integer number to an unnamed placeholder from the SQLCommand.
	@PARAM[cmd]: A valid SQLCommand object.
	@PARAM[n]: Position of the placeholder, starts on 0.
	@PARAM[num]: Value to bind (int).
	@RETS: True on success, false otherwise.
*/
int odk_sql_bind_int(odkSQLCommand *cmd, int n, int num);

/* @FUNC: odk_sql_bind_float
	@DESC: Binds a floating point number to an unnamed placeholder from the SQLCommand.
	@PARAM[cmd]: A valid SQLCommand object.
	@PARAM[n]: Position of the placeholder, starts on 0.
	@PARAM[num]: Value to bind (float).
	@RETS: True on success, false otherwise.
*/
int odk_sql_bind_float(odkSQLCommand *cmd, int n, float num);

/* @FUNC: odk_sql_bind_string
	@DESC: Binds a string to an unnamed placeholder from the SQLCommand.
	@PARAM[cmd]: A valid SQLCommand object.
	@PARAM[n]: Position of the placeholder, starts on 0.
	@PARAM[str]: Value to bind (string).
	@RETS: True on success, false otherwise.
*/
int odk_sql_bind_string(odkSQLCommand *cmd, int n, char *str);

/* @FUNC: odk_sql_bind_literal
	@DESC: Binds a string to an unnamed placeholder from the SQLCommand, but dosen't escapes the string.
	@PARAM[cmd]: A valid SQLCommand object.
	@PARAM[n]: Position of the placeholder, starts on 0.
	@PARAM[str]: Value to bind (string).
	@RETS: True on success, false otherwise.
*/
int odk_sql_bind_literal(odkSQLCommand *cmd, int n, char *str);

/* @FUNC: odk_sql_bind_blob
	@DESC: Binds a BLOB to an unnamed placeholder from the SQLCommand.
	@PARAM[cmd]: A valid SQLCommand object.
	@PARAM[n]: Position of the placeholder, starts on 0.
	@PARAM[size]: size in bytes of the blob.
	@PARAM[blob]: Value to bind (BLOB pointer).
	@RETS: True on success, false otherwise.
*/
int odk_sql_bind_blob(odkSQLCommand *cmd, int n, int size, void *blob);

/* @FUNC: odk_sql_bind_named
	@DESC: Binds a value to an named placeholder from the SQLCommand.
	@PARAM[cmd]: A valid SQLCommand object.
	@PARAM[pcn]: Placeholder name.
	@PARAM[val]: Value to bind (odkValue).
	@RETS: True on success, false otherwise.
*/
int odk_sql_bind_named(odkSQLCommand *cmd, char *pcn, odkValue *val);

/* @FUNC: odk_sql_bind_named_int
	@DESC: Binds a integer number to an named placeholder from the SQLCommand.
	@PARAM[cmd]: A valid SQLCommand object.
	@PARAM[pcn]: Placeholder name.
	@PARAM[num]: Value to bind.
	@RETS: True on success, false otherwise.
*/
int odk_sql_bind_named_int(odkSQLCommand *cmd, char *pcn, int num);

/* @FUNC: odk_sql_bind_named_float
	@DESC: Binds a floating point number to an named placeholder from the SQLCommand.
	@PARAM[cmd]: A valid SQLCommand object.
	@PARAM[pcn]: Placeholder name.
	@PARAM[num]: Value to bind.
	@RETS: True on success, false otherwise.
*/
int odk_sql_bind_named_float(odkSQLCommand *cmd, char *pcn, float num);

/* @FUNC: odk_sql_bind_named_string
	@DESC: Binds a string to an named placeholder from the SQLCommand.
	@PARAM[cmd]: A valid SQLCommand object.
	@PARAM[pcn]: Placeholder name.
	@PARAM[str]: Value to bind.
	@RETS: True on success, false otherwise.
*/
int odk_sql_bind_named_string(odkSQLCommand *cmd, char *pcn, char *str);

/* @FUNC: odk_sql_bind_named_literal
	@DESC: Binds a string to an named placeholder from the SQLCommand, but dosen't escapes the string.
	@PARAM[cmd]: A valid SQLCommand object.
	@PARAM[pcn]: Placeholder name.
	@PARAM[str]: Value to bind.
	@RETS: True on success, false otherwise.
*/
int odk_sql_bind_named_literal(odkSQLCommand *cmd, char *pcn, char *str);

/* @FUNC: odk_sql_bind_named_blob
	@DESC: Binds a BLOB to an named placeholder from the SQLCommand.
	@PARAM[cmd]: A valid SQLCommand object.
	@PARAM[pcn]: Placeholder name.
	@PARAM[size]: size in bytes of the blob.
	@PARAM[blob]: Value to bind (odkValue).
	@RETS: True on success, false otherwise.
*/
int odk_sql_bind_named_blob(odkSQLCommand *cmd, char *pcn, int size, void *blob);


/* @FUNC: odk_sql_prepare
	@DESC: Proccesses the SQL string, filling the placeholders with the respective value.
	Can be very useful for plugins where the native API dosen't support placeholders.
	@PARAM[cmd]: A valid SQLCommand object.
	@RETS: A prepared sql string on success, NULL otherwise.
*/
char *odk_sql_prepare(odkSQLCommand *cmd);

#endif 
