/*
ODKDA: Data Access Library
        Common Definitions
		
Copyright (C) 2006 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _ODKDA2_COMMON_H
#define _ODKDA2_COMMON_H

//Data Types
#define ODKDA_BOOL		1
#define ODKDA_CHAR		2
#define ODKDA_SHORT		3
#define ODKDA_INT		4
#define ODKDA_LONG		5
#define ODKDA_FLOAT		6
#define ODKDA_DOUBLE	7
#define ODKDA_TEXT		8
#define ODKDA_BLOB		9
#define ODKDA_DATE		10
#define ODKDA_TIME		11
#define ODKDA_DATETIME	12

/*enum DataType{
	BOOL = 1,
	CHAR,
	SHORT,
	INT,
	LONG,
	FLOAT,
	TEXT,
	BLOB,
	DATE,
	TIME,
	DATETIME
};*/

enum ColumnFlags{
	UNSIGNED,
	NOTNULL,
	ZEROFILL,
	PRIMARY_KEY,
	MULTIPLE_KEY,
	SERIAL
};

enum ProviderFeatues{
	SQL = 1,
	FILEDB,
	TRANSACTIONS,
	SSL,
	ROWCOUNT,
	FEATURE_BITCOUNT
};

enum SQLFeatures{
	SERIAL_FIELD,
	SQLFEATURE_BITCOUNT
};

//Column Flags
#define ODKDA_UNSIGNED		1
#define ODKDA_NOTNULL		2
#define ODKDA_ZEROFILL		4
#define ODKDA_PRI_KEY		8
#define ODKDA_MULTIPLE_KEY	16
#define ODKDA_AUTOINC		32

//Provider Features
#define ODKDA_SQL		1	//Can Execute SQL Commands
#define ODKDA_FILEDB		2	//Database on Disk File
#define ODKDA_TRANSACTIONS	3	//Supports Transactions
#define ODKDA_SSL		4	//Supports SSL Encripted Connections
#define ODKDA_ROWCOUNT		5	//Knows Query Row Count Before Fetching Results

#define ODKDA_FEATURE_BITCOUNT	5	//Feature Bit Count

//SQL Features
#define ODKDA_SQL_AUTOINC_FIELD	1
#define ODKDA_SQLFEATURE_BITCOUNT	1	//SQL Feature Bit Count

#endif
