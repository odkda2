/*
ODKDA: Data Access Library
        Base Command Object
		
Copyright (C) 2006 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _COMMAND_H
#define _COMMAND_H

#define ODK_STATE_NEW		0
#define ODK_STATE_EXECUTED	1
#define ODK_STATE_READY		2
#define ODK_STATE_FETCHED	3

#define ODK_STRING_PTR		1

typedef struct {
	int size;
	int type;
	int flags;
} odkFieldDef;

typedef odkValue **(ODKAPI *FUNC_COM_FETCH) (void *com);
typedef int (ODKAPI *FUNC_COM_FETCHINTO) (void *com, odkValue ** row);
typedef int (ODKAPI *FUNC_COM_FETCHEX_INTO) (void *com, int n, odkFieldDef *types, void *ptrs);

typedef struct {
    int type;			//Command Type
	int state;
    void *private_data;		//Command Private Data

    odkColumn **columns;	//Column Description
    int colCount;		//Column Count
    int rowCount;		//Row Count
    int affected;		//Affected Records

    FUNC_COM_FETCH 			fetch;	//Fetch one row
    FUNC_COM_FETCHINTO 		fetch_into;	//Fetch one row into already allocated memory
	FUNC_COM_FETCHEX_INTO 		fetchex_into; //Extended Fetch
} _odkCommand;

#define CLASS_COMMAND CLASS_OBJECT _odkCommand Class_Command;

typedef struct {
CLASS_COMMAND
} odkCommand;

#define COMMAND(o) (((odkCommand*)o)->Class_Command)

/* @FUNC: odk_command_colcount
	@DESC: Returns the number of columns of the resultset.
	@PARAM[c]: A valid Command object (can be an SQLCommand).
	@RETS: Number of columns of the resultset.
*/
int odk_command_colcount(odkCommand * c);

/* @FUNC: odk_command_columns
	@DESC: Returns an array of odkColumn objects describing the columns of the resultset.
	@PARAM[c]: A valid Command object (can be an SQLCommand).
	@RETS: An array of odkColumn objects.
*/
odkColumn **odk_command_columns(odkCommand * c);

/* @FUNC: odk_command_fetch_into
	@DESC: Fetches a row from the resultset.
	@PARAM[c]: A valid Command object (can be an SQLCommand).
	@PARAM[row]: An array of odkValue objects previously initialized, must but colcount of size.
	@RETS: True on success, false otherwise.
*/
int odk_command_fetch_into(odkCommand * c, odkValue ** row);

/* @FUNC: odk_command_fetch
	@DESC: Returns an array of odkValue objects containing the values of the fields in
	the current row.
	@PARAM[c]: A valid Command object (can be an SQLCommand).
	@RETS: An array of odkColumn objects.
*/
odkValue **odk_command_fetch(odkCommand * c);

/* @FUNC: odk_command_fetchex
	@DESC: Extended fetch, fetches the current row to a structure or memory segment.
	@PARAM[c]: A valid Command object (can be an SQLCommand).
	@PARAM[n]: Number of fields inside the structure.
	@PARAM[type]: Definition of the structure fields.
	@RETS: A pointer to new structure, NULL otherwise.
*/
void* odk_command_fetchex(odkCommand * c, int n, odkFieldDef *type);

/* @FUNC: odk_command_fetchex_into
	@DESC: Extended fetch, fetches the current row to a structure or memory segment previously allocated.
	@PARAM[c]: A valid Command object (can be an SQLCommand).
	@PARAM[n]: Number of fields inside the structure.
	@PARAM[type]: Definition of the structure fields.
	@PARAM[ptr]: A pointer to the structure (previously allocated).
	@RETS: 0 on success error code otherwise.
*/
int odk_command_fetchex_into(odkCommand * c, int n, odkFieldDef *type, void *ptr);

/* @FUNC: odk_command_init
	@DESC: Initializes the Command instance.
	@PARAM[o]: A valid Command object.
	@RETS: An array of odkColumn objects.
*/
void odk_command_init(void *o);

/* @FUNC: odk_command_new
	@DESC: Creates a new Command object.
	@RETS: A valid Command object.
*/
odkCommand *odk_command_new();

#endif
