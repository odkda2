/*
ODKDA: Data Access Library
        Connection Object
		
Copyright (C) 2006 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _CONNECTION_H
#define _CONNECTION_H

typedef int (ODKAPI *FUNC_CONN_FREE) (void *conn);
typedef int (ODKAPI *FUNC_CONN_OPEN) (void *conn, char *host, int port, char *user,
			      char *passwd, char *db);
typedef int (ODKAPI *FUNC_CONN_CLOSE) (void *conn);
typedef int (ODKAPI *FUNC_CONN_EXEC) (void *conn, void *cmd);

typedef int (ODKAPI *FUNC_TRANSAC_FUNC) (void *conn);

typedef struct {
    odkDataProvider *parent;
    FUNC_CONN_OPEN open;
    FUNC_CONN_CLOSE close;
    FUNC_CONN_EXEC execute;
    FUNC_TRANSAC_FUNC begin;
    FUNC_TRANSAC_FUNC rollback;
    FUNC_TRANSAC_FUNC commit;
} _odkConnection;

#define CLASS_CONNECTION CLASS_OBJECT _odkConnection Class_Connection;

typedef struct {
CLASS_CONNECTION} odkConnection;

#define CONNECTION(o) (((odkConnection*)o)->Class_Connection)

/* @FUNC: odk_connection_new
	@DESC: Creates a new connection.
	@PARAM[dp]: A valid DataProvider object.
	@RETS: A valid and closed connection to the provider.
*/
odkConnection *odk_connection_new(odkDataProvider * dp);

/* @FUNC: odk_connection_open
	@DESC: Opens the connection to the database.
	@PARAM[conn]: A valid Connection object.
	@PARAM[host]: Hostname or IP address of the database server, NULL for default.
	@PARAM[port]: Number of the port, 0 for default.
	@PARAM[user]: Username to use for authenticating with the database server.
	@PARAM[passwd]: Password to use for authenticating with the database server.
	@PARAM[db]: Name of the database to connect to, can be a filename for file based data providers.
	@RETS: True on success, false otherwise.
*/
int odk_connection_open(odkConnection * conn, char *host, int port, char *user, char *passwd, char *db);

/* @FUNC: odk_connection_close
	@DESC: Closes the connection object.
	@PARAM[conn]: A valid Connection object.
	@RETS: True on success.
*/
int odk_connection_close(odkConnection * conn);

/* @FUNC: odk_connection_execute
	@DESC: Sends and executes a command on the database.
	@PARAM[conn]: A valid Connection object.
	@PARAM[cmd]: A valid Command object (can be an SQLCommand).
	@RETS: True on success.
*/
int odk_connection_execute(odkConnection * conn, odkCommand * cmd);

/* @FUNC: odk_transaction_begin
	@DESC: Starts a new transaction if supported.
	@PARAM[conn]: A valid Connection object.
	@RETS: True on success.
*/
int odk_transaction_begin(odkConnection * conn);

/* @FUNC: odk_transaction_rollback
	@DESC: Rollbacks the current transaction.
	@PARAM[conn]: A valid Connection object.
	@RETS: True on success.
*/
int odk_transaction_rollback(odkConnection * conn);

/* @FUNC: odk_transaction_commit
	@DESC: Commits the current transaction.
	@PARAM[conn]: A valid Connection object.
	@RETS: True on success.
*/
int odk_transaction_commit(odkConnection * conn);

#endif
