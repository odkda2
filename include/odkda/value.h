/*
ODKDA: Data Access Library
        Value Object
		
Copyright (C) 2006 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _VALUE_H
#define _VALUE_H

typedef struct {
    CLASS_OBJECT 
	char type;
    int size;
	char changed;
	
    union {
	int vBool;
	char vChar;
	short vShort;
	int vInt;
	long vLong;
	char *vString;
	float vFloat;
	double vDouble;
	void *vBlob;
	odkDate *vDate;
	odkTime *vTime;
	odkDateTime *vDateTime;
    };
} odkValue;

odkValue *odk_value_new(char type);
odkValue *odk_value_new_shared(char type, odkMemPool * m);

int odk_value_set(odkValue * v, char *val);
int odk_value_set_bool(odkValue * v, bool val);
int odk_value_set_char(odkValue * v, char val);
int odk_value_set_short(odkValue * v, short val);
int odk_value_set_int(odkValue * v, int val);
int odk_value_set_long(odkValue * v, long val);
int odk_value_set_float(odkValue * v, float val);
int odk_value_set_double(odkValue * v, double val);
int odk_value_set_blob(odkValue * v, int size, void *val);
int odk_value_set_date(odkValue * v, odkDate * val);
int odk_value_set_time(odkValue * v, odkTime * val);
int odk_value_set_datetime(odkValue * v, odkDateTime * val);

char *odk_value_get(odkValue * v);
bool odk_value_get_bool(odkValue * v);
char odk_value_get_char(odkValue * v);
short odk_value_get_short(odkValue * v);
int odk_value_get_int(odkValue * v);
long odk_value_get_long(odkValue * v);
float odk_value_get_float(odkValue * v);
double odk_value_get_double(odkValue * v);
void *odk_value_get_blob(odkValue * v);
odkDate *odk_value_get_date(odkValue * v);
odkTime *odk_value_get_time(odkValue * v);
odkDateTime *odk_value_get_datetime(odkValue * v);

int odk_value_get_type(odkValue *v);
int odk_value_get_size(odkValue *v);
bool odk_value_is_changed(odkValue *v);

#endif
