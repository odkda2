/*
ODKDA: Data Access Library
        Data Provider Object
		
Copyright (C) 2006 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _PROVIDER_H
#define _PROVIDER_H

typedef void *(ODKAPI *FUNC_CONNECTION_NEW) ();
typedef int (ODKAPI *FUNC_INIT) (void *dp);

typedef struct {
    odkLibHandle *plugin;
    odkBitfield features;
    odkBitfield sqlfeatures;

    odkSList *extensions;

    FUNC_CONNECTION_NEW conn_new;
} _odkDataProvider;

#define CLASS_DATAPROVIDER CLASS_OBJECT _odkDataProvider Class_odkDataProvider;

typedef struct {
CLASS_DATAPROVIDER} odkDataProvider;

#define DATAPROVIDER(o) (o->Class_odkDataProvider)

/* @FUNC: odk_dataprovider_enum
	@DESC: Enumerates available DataProviders.
	@RETS: A List (odkSList) with the available DataProviders.
*/
odkSList *odk_dataprovider_enum();

/* @FUNC: odk_dataprovider_new
	@DESC: Creates a new DataProvider.
	@PARAM[provider]: Name of the Data Provider to load.
	@RETS: A valid DataProvider object.
*/
odkDataProvider *odk_dataprovider_new(char *provider);

/* @FUNC: odk_dataprovider_sql_supports
	@DESC: Checks if a given SQL Lenguage feature is available on the given DataProvider.
	@PARAM[dp]: A valid DataProvider object.
	@PARAM[feature]: Feature to check.
	@RETS: True if the feature is supported, false otherwise.
*/
int odk_dataprovider_sql_supports(odkDataProvider * dp, int feature);

/* @FUNC: odk_dataprovider_supports
	@DESC: Checks if a given feature is available on the given DataProvider.
	@PARAM[dp]: A valid DataProvider object.
	@PARAM[feature]: Feature to check.
	@RETS: True if the feature is supported, false otherwise.
*/
int odk_dataprovider_supports(odkDataProvider * dp, int feature);

/* @FUNC: odk_dataprovider_extension
	@DESC: Loads a given DataProvider extension.
	@PARAM[dp]: A valid DataProvider object.
	@PARAM[ext]: Name of the extension.
	@RETS: A pointer to the extension function, NULL otherwise.
*/
void *odk_dataprovider_extension(odkDataProvider * dp, char *ext);

/* @FUNC: odk_dataprovider_has_extension
	@DESC: Checks if a given extension is available on the DataProvider.
	@PARAM[dp]: A valid DataProvider object.
	@PARAM[ext]: Name of the extension.
	@RETS: True if the extension is available, false otherwise.
*/
bool odk_dataprovider_has_extension(odkDataProvider * dp, char *ext);

/* @FUNC: odk_dataprovider_extlist
	@DESC: Enumerates the available extensions on a given DataProvider.
	@PARAM[dp]: A valid DataProvider object.
	@RETS: A List (odkSList) with the available extensions.
*/
odkSList *odk_dataprovider_extlist(odkDataProvider * dp);

#endif
