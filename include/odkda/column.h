/*
ODKDA: Data Access Library
        Column Object
		
Copyright (C) 2006 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _COLUMN_H
#define _COLUMN_H 

typedef struct {
    CLASS_OBJECT 
	char *name;
    int type;
    int maxSize;
    int flags;
} odkColumn;

#define COLUMN(o) (*(odkColumn*)o)

odkColumn *odk_column_new(char *name);
odkColumn *odk_column_new1(char *name, int type, int flags);

int odk_column_get_type(odkColumn *c);
int odk_column_get_flags(odkColumn *c);

void odk_column_set_type(odkColumn *c, int type);
void odk_column_set_flags(odkColumn *c, int flags);

#endif
