/*
ODKDA: Data Access Library
        Select Command Object
		
Copyright (C) 2006 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _SELECTCMD_H
#define _SELECTCMD_H

#define ODKDA_ORDER_ASC		1
#define ODKDA_ORDER_DESC	2

typedef struct{
	char *column;
	char order;
}odkOrderColumn;

typedef struct{
	CLASS_COMMAND
	char *table;
	odkSList *columns;
	odkSList *orders;
	int limit;
	int offset;	
} odkSelectCommand;

odkSelectCommand *odk_select_new(char *table);
odkSelectCommand *odk_select_new1();

int odk_select_add_column(odkSelectCommand *c, char *column, char *table);
int odk_select_add_order(odkSelectCommand *c, char *column, char order);
int odk_select_set_limit(odkSelectCommand *c, int limit);
char *odk_select_tosql(odkSelectCommand *c);

#endif
