/*
ODKDA: Data Access Library
        Schema Object
		
Copyright (C) 2006 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _SCHEMA_H
#define _SCHEMA_H

typedef struct {
	char *name;
    odkList *columns;
    odkColumn *key;
    odkList *defaults;
} _Schema;

#define CLASS_SCHEMA CLASS_OBJECT _Schema Class_Schema;
#define SCHEMA(o) (o->Class_Schema)

typedef struct {
CLASS_SCHEMA
} odkSchema;

odkSchema *odk_schema_new(char *name);

int odk_schema_colcount(odkSchema * schm);
odkList *odk_schema_columns(odkSchema * schm);
odkColumn *odk_schema_get_column(odkSchema * schm, int n);
odkColumn *odk_schema_get_key(odkSchema * schm);

#endif
