/*
ODKDA: Data Access Library
        Common Extensions Definitions
		
Copyright (C) 2006 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _EXTS_H
#define _EXTS_H

/* change_db
   This function should change the current database in use,
   return false if failture.

Proto:
   int ext_change_db(odkConnection *conn, char *db)
   
conn: A valid odkConnection Object.
db: Name of the desired databse to change
*/
typedef int (ODKAPI *ext_change_db_func) (odkConnection * conn, char *db);


/* table_list
   This function should retrive a list of available
   tables from current database, the list elements
   are strings, is responsability of the developer
   to free them along with the list.

Proto:
   odkSList* ext_table_list(odkConnection *conn)
   
conn: A valid odkConnection Object.
*/
typedef odkSList *(ODKAPI *ext_table_list_func) (odkConnection * conn);


/* table_schema
   This function should retrive the schema data of a given
   table from current database.

Proto:
   odkSchema* ext_table_schema(odkConnection *conn, char *table)
   
conn: A valid odkConnection Object.
table: Name of the desired table to retrive schema.
*/
typedef odkSchema *(ODKAPI *ext_table_schema_func) (odkConnection * conn,
					    char *table);

typedef struct {
	char version;
	char major;
	char minor;
	
	ext_change_db_func 		ext_change_db;
	ext_table_list_func 	ext_table_list;
	ext_table_schema_func 	ext_table_schema;
} odkExt;


/* @FUNC: odk_ext_autoload
	@DESC: Automatically loads all available extensions on a given DataProvider,
	Adding them to a odkExt structure with all functions pointers, NULL where
	the extension is not supported.
	@PARAM[dp]: A valid DataProvider object.
	@RETS: An odkExt object.
*/
odkExt *odk_ext_autoload(odkDataProvider *dp);

#endif
