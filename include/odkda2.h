/*
ODKDA: Data Access Library
		
Copyright (C) 2006 Carlos Daniel Ruvalcaba Valenzuela
	Contact me: <clsdaniel@gmail.com>
					
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _ODKDA2_H
#define _ODKDA2_H

#if defined(_MSC_VER)
//#pragma comment(lib, \"libodkda2.lib\")
#endif

#ifdef __cplusplus
extern "C" {
#endif

#include <odkda/common.h>
#include <odkda/date.h>
#include <odkda/time.h>
#include <odkda/datetime.h>
#include <odkda/value.h>
#include <odkda/column.h>
#include <odkda/schema.h>
#include <odkda/command.h>
#include <odkda/selectcmd.h>
#include <odkda/sqlcommand.h>
#include <odkda/provider.h>
#include <odkda/connection.h>
#include <odkda/exts.h>

#ifdef __cplusplus
}
#endif

#endif
